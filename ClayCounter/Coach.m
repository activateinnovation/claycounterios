//
//  Coach.m
//  ClayCounter
//
//  Created by Test on 1/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "Coach.h"


@implementation Coach

@dynamic fName;
@dynamic lName;
@dynamic phone;
@dynamic userId;
@dynamic leftRightHanded;
@dynamic objectId;
@dynamic email;

@end
