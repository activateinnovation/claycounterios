//
//  StoredShoot.m
//  ClayCounter
//
//  Created by Test on 2/27/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "StoredShoot.h"


@implementation StoredShoot

@dynamic coachId;
@dynamic userId;
@dynamic typeShoot;
@dynamic totalShots;
@dynamic objectId;
@dynamic shotsPerRotation;
@dynamic dateCreated;
@dynamic trapFieldId;
@dynamic trapHouseNumber;
@dynamic handicap;

@end
