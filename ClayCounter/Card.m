//
//  Card.m
//  ClayCounter
//
//  Created by Test on 4/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "Card.h"

@interface Card ()

@end

@implementation Card
@synthesize scoringPage;
@synthesize webView;
@synthesize shootersArray;
@synthesize shooterShootObjectsArray;
@synthesize index1;
@synthesize spinner;


@synthesize storedShoot;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSLog(@"HERE");
  
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];

    
    

    
}

-(BOOL) prefersStatusBarHidden
{
    return YES;
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
   
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return  interfaceOrientation == UIInterfaceOrientationLandscapeLeft
    || interfaceOrientation == UIInterfaceOrientationLandscapeRight ;
}

- (void)viewDidLoad
{
    
    self.shootersArray = [[NSMutableArray alloc] init];
    self.shooterShootObjectsArray = [[NSMutableArray alloc] init];
    
    self.reorederShootData = [[NSMutableArray alloc] init];

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];

    
    if(self.view.frame.size.width > 600){
        
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 40, 1000, 700)];
        
        
    }
    else{
        
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 40, 575, 280)];
    }
    
    self.webView.delegate = self;
    [self.webView setUserInteractionEnabled:YES];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
    [self.view addSubview: self.webView];
    
    [spinner startAnimating];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    

    
    
    [super viewDidLoad];
     [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
   
    // Dispose of any resources that can be recreated.
}

//Rotates the view to landscape
-(void)showScoreCard : (int) index : (NSMutableArray *)shootShooters : (NSMutableArray *)shooterShootObjects : (StoredShoot *)storedShootTemp {
    
    [self.view setTransform:CGAffineTransformMakeRotation(-1.57)];
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    self.index1 = index;
    self.shootersArray = shootShooters;
    self.shooterShootObjectsArray = shooterShootObjects;
    self.storedShoot = storedShootTemp;
    
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    dateString = [formatter stringFromDate: self.storedShoot.dateCreated];
    
    
    TrapField *trapField;
    NSString *trapFieldString = [[NSString alloc] init];
    
    if([storedShoot.trapFieldId isEqualToString:@"NA"]){
        
        trapFieldString = @"No Range Recorded";
    }
    else{
    
        trapField = [TrapField MR_findFirstByAttribute:@"objectId" withValue:self.storedShoot.trapFieldId];
        trapFieldString = trapField.name;
    }
    

    self.titleInfoLabel.text = [NSString stringWithFormat:@"%@- %@- %@ Shots- %@ yrds-\nHouse #%@- %@",dateString , self.storedShoot.typeShoot, self.storedShoot.totalShots, self.storedShoot.handicap, self.storedShoot.trapHouseNumber, trapFieldString];
    //ShooterShootObject *firstShooterObject = [self.shooterShootObjectsArray objectAtIndex:0];
   
    //NEED TO ADD THE SHOOT ID TO THE SHOOT SHOOTERS IN CORE DATA SO THAT WE CAN GET THE NUMBER OF SHOTS FOR POST IF THE SHOOT HAS 10 AT EACH POST. REORDER DATA WILL NEED TO BE UPDATED TO PUT THE 10 SHOTS IN A ROW TO THE RIGHT PLACE, MAYBE FIRSTHALF AND SECONDHALF
   // self.storedShoot = [StoredShoot MR_findFirstByAttribute:@"objectId" withValue: firstShooterObject.]
   // NSLog(@"Shooters: %@", self.shootersArray);
    //NSLog(@"ShooterShootObjects: %@", self.shooterShootObjectsArray);
   // NSLog(@"StoredShoot: %@", self.storedShoot);
    
    
    [self reorderData];
   // NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
    //[webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    /*NSURL *url = [[NSURL alloc] initWithString:@"index.html"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url];
    [self.webView loadRequest:request];*/
}

- (IBAction)cancel:(id)sender {
    
    
    [self presentViewController:self.scoringPage animated:YES completion:nil];
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"page is loading");
    //NSLog(@"reorderedData: %@", self.reorederShootData);
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"finished loading");
    

    [self sendShooter];
    [spinner stopAnimating];
   
}

-(void) sendShooter{
    
    
    for(int x = 0; x < shooterShootObjectsArray.count; x++){
        
   // ShootShooters *tempShootShooter = [shooterShootObjectsArray objectAtIndex:x];
    Shooter *tempShooter = self.shootersArray[x];
    NSString *shooterName = [NSString stringWithFormat:@"%@ %@", tempShooter.fName, tempShooter.lName];
    NSString *scoreData = [self.reorederShootData[x] componentsJoinedByString:@","];
    ShootShooters *shootShooter = [self.shooterShootObjectsArray objectAtIndex: x];
    
       
   // NSString *temp = [self.reorederShootData componentsJoinedByString:@","];
    NSString *function = [NSString stringWithFormat: @"getData('%@', '%@', %d, %d);", scoreData, shooterName, [storedShoot.shotsPerRotation intValue], [shootShooter.totalHits intValue]];
    NSString *result = [self.webView stringByEvaluatingJavaScriptFromString: function];
    NSLog(@"Result: %@", result);

    }
    
}

-(void) reorderData {
    
     NSLog(@"here");
    
    
    
  /*  if(shooterShootObjectsArray.count == 1){
    ShootShooters *temp = [shooterShootObjectsArray objectAtIndex:0];
    NSArray * tempList = [temp.scoreData componentsSeparatedByString:@","];
    NSMutableArray *scoreData  = [NSMutableArray arrayWithArray: tempList];
    //NSLog(@"ScoreData: %@", scoreData);
        
    //This data stores the shooters scores in order after they have been rearane
    [self.reorederShootData addObject:scoreData];

    }
    else{*/
    
    for(int x = 0; x < shooterShootObjectsArray.count; x++)
    {
        
        ShootShooters *temp = [shooterShootObjectsArray objectAtIndex:x];
        
        //  NSLog(@"SCORE DATA As String: %@", temp.scoreData);
        NSArray * tempList = [temp.scoreData componentsSeparatedByString:@","];
    
        
        NSMutableArray *scoreData  = [NSMutableArray arrayWithArray: tempList];
        
       // NSLog(@"SCORE DATA BEFORE: %@", scoreData);
        
      
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:scoreData];
     

     //   NSLog(@"ShooterData: %@ Starting Post: %@", scoreData, temp.startingPost);
        
        for(int y = 0; y < scoreData.count; y++){
            
            
            if(scoreData.count == 25){
                
                int pos = y + (([temp.startingPost intValue] - 1) * [self.storedShoot.shotsPerRotation intValue]);
                
                if(pos >= scoreData.count){
                    
                    pos = pos % scoreData.count;
                    // NSLog(@"pos: %d", pos);
                    
                }
                
                [tempArray replaceObjectAtIndex: pos withObject: scoreData[y]];
                
            }

            else{
             
                int pos = y + (([temp.startingPost intValue] - 1) * [self.storedShoot.shotsPerRotation intValue]);
                if(pos >= scoreData.count){
                    
                    pos = pos % scoreData.count;
                    //NSLog(@"pos: %d", pos);
                    
                }
                [tempArray replaceObjectAtIndex: pos withObject: scoreData[y]];
                
            }
            
        }
        
       // NSLog(@"TempArray: %@", tempArray);
        
        [self.reorederShootData addObject:tempArray];
        
       // NSLog(@"REORDERED DATA: %@", self.reorederShootData);
        
    }
    
   // }
    
}


- (IBAction)sendEmail:(id)sender {
    
    
    
    Coach *scoringCoach = [Coach MR_findFirstByAttribute:@"objectId" withValue:self.storedShoot.coachId];
    
    NSArray *coachesArray = [Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"  ascending:YES];
    
    NSMutableArray *tempEmailArray = [[NSMutableArray alloc] init];
    for(int x = 0; x < shootersArray.count; x++)
    {
        Shooter *temp = [shootersArray objectAtIndex:x];
        [tempEmailArray addObject:temp.email];
    }
    
    for(int x = 0; x < coachesArray.count; x++){
        
        Coach *temp = [coachesArray objectAtIndex:x];
        [tempEmailArray addObject:temp.email];
        
    }
    
    NSArray *finalEmails = [[NSArray alloc] initWithArray:tempEmailArray];
    
   /* NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    dateString = [formatter stringFromDate:self.storedShoot.dateCreated];
    
    TrapField *trapField;
    NSString *trapFieldString = [[NSString alloc] init];
    
    if([storedShoot.trapFieldId isEqualToString:@"NA"]){
        
        trapFieldString = @"No Range Recorded";
    }
    else{
        
        trapField = [TrapField MR_findFirstByAttribute:@"objectId" withValue:self.storedShoot.trapFieldId];
        trapFieldString = trapField.name;
    }
    
    
    self.titleInfoLabel.text = [NSString stringWithFormat:@"%@- %@- House #%@\n%@",dateString , self.storedShoot.typeShoot, self.storedShoot.trapHouseNumber, trapFieldString];*/
    
    int squadTotal = 0;
    int squadTotalShots = 0;
    NSString *scoresString = @"";
    for(int x = 0; x < self.shooterShootObjectsArray.count; x++)
    {
        
        ShootShooters *temp = self.shooterShootObjectsArray[x];
        Shooter *tempShooter = [Shooter MR_findFirstByAttribute:@"objectId" withValue:temp.shooterId];
        squadTotalShots += [temp.totalShots intValue];
        squadTotal += [temp.totalHits intValue];
        scoresString = [NSString stringWithFormat:@"%@%@ %@ Hit: %@/%@</br>", scoresString, tempShooter.fName, tempShooter.lName, temp.totalHits, temp.totalShots];
        
    }
    
    
    NSLog(@"Squad Total: %d", squadTotal);
    NSLog(@"scoreString: %@", scoresString);
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSString *html = [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    [picker setSubject:self.titleInfoLabel.text];
    [picker setToRecipients: finalEmails];
    [picker setMessageBody:[NSString stringWithFormat:@"<h4><b>%@ %@ scored this shoot.</b></h4><p><b>Handicap: %@ yrds</b></p><h4><b>Squad Total: %d/%d Shots</b></h4><p><b>%@</b></p></br> %@", scoringCoach.fName, scoringCoach.lName, self.storedShoot.handicap, squadTotal, squadTotalShots, scoresString, html] isHTML:YES];
    
   // [self presentViewController:picker animated:YES completion:nil];
  
    if([MFMailComposeViewController canSendMail] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
 
    }
    else if([self isNetworkAvailable] == NO){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"There is no network connection available to send emails currently." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
 
    }
    else if([self isNetworkAvailable] == NO && [MFMailComposeViewController canSendMail]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"These is no network connection and your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        

    }
    else{
        
        [self presentViewController:picker animated:YES completion:nil];
    }
        
            
  
    
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    [self dismissViewControllerAnimated:YES completion: ^{
        
        [self.view setTransform:CGAffineTransformMakeRotation(-1.57)];
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
        
        if(result == MFMailComposeResultSent){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Scores Sent" message:@"These scores were successfully sent." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if(result == MFMailComposeResultFailed)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failes" message:@"These scores failed to send." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            
        }
     
       
        
    }];
 
}

            
-(BOOL) isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
