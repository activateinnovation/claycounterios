//
//  CustomShootCell.h
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
@protocol CustomShootCellDelegate <NSObject>

-(void)scoreFieldClicked : (int)cellTag :(UITextField *)textField :(NSMutableArray *)shootShooter;


@end

#import "ScoringShootPage.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>



@class ScoringShootPage;

@interface CustomShootCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) ScoringShootPage *scoringShootPage;
@property (strong, nonatomic) NSMutableArray *textFieldArray;
@property (weak, nonatomic) id<CustomShootCellDelegate> delegate;
@property (strong, nonatomic) UILabel *totalShotsInShootLabel;
@property (strong, nonatomic) UILabel *shotsCounter;
@property int shotsInShootCounter;
@property int shotsFired;
@property (strong, nonatomic) UILabel *postCounter;
@property int postInShootCounter;
@property int hitsOverall;
@property int shotsAtEachPost;
@property (strong, nonatomic) UILabel *hitsOverallLabel;
@property (strong, nonatomic) NSMutableArray *scoreArray;
@property int shotsFiredInRound;
@property int totalShotsInShoot;
@property int roundNumber;
@property int postNumber;

-(void) clearTextFields;
@property (strong, nonatomic) UILabel *roundLabel;
@property (strong, nonatomic) NSMutableArray *tempScores;
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier :(NSString *) totalShots :(NSString *)shotsFired :(NSString *)post :(NSString *)shootername :(NSNumber *)shotsAtEachPostTemp :(int)hitsOverallTemp : (NSMutableArray *)shooterScoreArray : (int)roundNumberTemp :(int)shotsFiredInRoundTemp : (NSMutableArray *)shootShooter;

//-(void) setUpWithShooter : (NSString *) totalShots : (NSString *)  shotsFired : (NSString *)post;
@property (strong, nonatomic) UILabel *shooterName;
@property (strong, nonatomic) UITextField *textField0;
@property (strong, nonatomic) UITextField *textField1;
@property (strong, nonatomic) UITextField *textField2;
@property (strong, nonatomic) UITextField *textField3;
@property (strong, nonatomic) UITextField *textField4;
@property (strong, nonatomic) UITextField *textField5;
@property (strong, nonatomic) UITextField *textField6;
@property (strong, nonatomic) UITextField *textField7;
@property (strong, nonatomic) UITextField *textField8;
@property (strong, nonatomic) UITextField *textField9;

@property(strong, nonatomic) NSMutableArray *shootShooterArray;


@end
