//
//  CurrentShoot.h
//  ClayCounter
//
//  Created by Test on 2/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CurrentShoot : NSManagedObject

@property (nonatomic, retain) NSString * coach;
@property (nonatomic, retain) NSString * shooters;
@property (nonatomic, retain) NSNumber * totalShots;
@property (nonatomic, retain) NSNumber * shotsAtPost;
@property (nonatomic, retain) NSString * pracOrComp;
@property (nonatomic, retain) NSString * startingPost;
@property (nonatomic, retain) NSString *trapFieldId;
@property (nonatomic, retain) NSNumber *trapHouseNumber;
@property (nonatomic, retain) NSNumber * handicap;

@end
