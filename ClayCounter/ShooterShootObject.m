//
//  ShooterShootObject.m
//  ClayCounter
//
//  Created by ActivateInnovation on 2/9/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ShooterShootObject.h"

@implementation ShooterShootObject
@synthesize shooter;
@synthesize postNumber;
@synthesize shotCounter;
@synthesize hitNumber;
@synthesize scoreArray;
@synthesize shotsFiredInRound;
@synthesize hitsInRound;

-(ShooterShootObject *) initWithItems: (Shooter *)shooterTemp : (int)postNumberTemp : (int) shotCounterTemp : (int) hitNumberTemp : (NSMutableArray *)scoreArrayTemp : (int)shotsFiredInRoundTemp :(int) hitsInShootTemp {
    
    self.shotsFiredInRound = shotsFiredInRoundTemp;
    self.shooter = shooterTemp ;
    self.postNumber = postNumberTemp;
    self.shotCounter = shotCounterTemp;
    self.hitNumber = hitNumberTemp;
    self.scoreArray = scoreArrayTemp;
    self.hitsInRound = hitsInShootTemp;
    return self;
}







@end
