//
//  AppDelegate.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "ShootersPage.h"
#import "UpdatedData.h"

@class ShootersPage;
@class UpdatedData;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
    
   // NSManagedObjectModel *managedObjectModel;
  //NSManagedObjectContext *managedObjectContext;
   // NSPersistentStoreCoordinator *persistentStoreCoordinator;


@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property ShootersPage *shooterPage;
@property UpdatedData *updatedData;
//@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
//@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
//@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UIWindow *window;

@end
