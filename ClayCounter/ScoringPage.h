//
//  ScoringPage.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShootersPage.h"
#import "SettingsPage.h"
#import "SetupShootView.h"
#import "StoredShoot.h"
#import "ShootShooters.h"
#import "Shooter.h"
#import "Coach.h"
#import "Card.h"
#import "UpdatedData.h"

@class ShootersPage;
@class SettingsPage;
@class SetupShootView;
@class UpdatedData;
@class Card;

@interface ScoringPage : UIViewController <UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property ShootersPage *shooterPage;
@property SettingsPage *settingsPage;
@property SetupShootView *setupShootView;
@property Card *card;
@property UpdatedData *updatedData;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) NSManagedObjectContext *managedContext;

@property NSIndexPath *indexPathToDelete;
- (IBAction)setUpNewShoot:(id)sender;
@property (strong, nonatomic) NSMutableArray *storedShootArray;
@property (strong, nonatomic) NSMutableArray *shootCoachesArray;
@property (strong, nonatomic) NSMutableArray *shootShootersArray;
@property (strong, nonatomic) NSMutableArray *shootShootersObjects;
@property (strong, nonatomic) StoredShoot *shootToDelete;
@end
