//
//  SignUpView.h
//  ClayCounter
//
//  Created by Test on 1/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShootersPage.h"
#import "LoginView.h"
#import "UpdatedData.h"
#import "Coach.h"

@class LoginView;
@class ShootersPage;
@class UpdatedData;
@class Coach;

@interface SignUpView : UIViewController <UIScrollViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate>
@property UpdatedData *updateData;
@property (strong, nonatomic) NSMutableArray *trapFieldsArray;
@property (strong, nonatomic) NSString *trapFieldId;
- (IBAction)teamTrapRangeClicked:(id)sender;
- (IBAction)usernameCheckDidEnd:(id)sender;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (strong, nonatomic) IBOutlet UILabel *rightHandedLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftHandedLabel;
@property (strong, nonatomic) IBOutlet UISwitch *leftRightSwitch;
@property (strong, nonatomic) IBOutlet UITextView *decriptionTextView;
@property (strong, nonatomic) UIActionSheet *menu;
- (IBAction)checkEmailDone:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *checkmarkImageView2;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong , nonatomic) NSMutableArray *tempSignUpData;
@property (strong, nonatomic) IBOutlet UILabel *teamInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *headCoachLabel;
@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) PFObject *coach;
@property ShootersPage *shooterPageView;
@property LoginView *loginView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UITextField *orgNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *homeTrapRangeTextField;
@property (strong, nonatomic) IBOutlet UITextField *headCoachFName;
@property (strong, nonatomic) IBOutlet UITextField *headCoachLName;
@property int counter;
@property (strong, nonatomic) IBOutlet UIButton *NextSelected;
@property (strong ,nonatomic) PFObject *trapFieldSelected;
@property (strong, nonatomic) IBOutlet UITextField *headCoachPhoneNum;
@property (strong, nonatomic) IBOutlet UITextField *headCoachEmail;
- (IBAction)nextSelectedAction:(id)sender;
@property (strong, nonatomic) PFUser *user;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *repeatPasswordTextField;
- (IBAction)cancelSignUp:(id)sender;
-(IBAction)keyboardNext:(id)sender;
-(IBAction)keyboardDone:(id)sender;
@end
