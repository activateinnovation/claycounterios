//
//  Shooter.m
//  ClayCounter
//
//  Created by Test on 1/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "Shooter.h"


@implementation Shooter

@dynamic choke;
@dynamic email;
@dynamic emergencyContact;
@dynamic emergencyContactNum;
@dynamic fName;
@dynamic lName;
@dynamic phone;
@dynamic userId;
@dynamic objectId;

-(Shooter *)initWithName : (NSString *) firstName : (NSString *) lastName
{
    
    
    self.fName = firstName;
    self.lName = lastName;
    
    
    return self;
    
}



@end
