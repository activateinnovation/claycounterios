//
//  ShootShooters.m
//  ClayCounter
//
//  Created by Test on 2/27/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ShootShooters.h"


@implementation ShootShooters

@dynamic shooterId;
@dynamic storedShootId;
@dynamic scoreData;
@dynamic totalShots;
@dynamic totalHits;
@dynamic userId;
@dynamic startingPost;

@end
