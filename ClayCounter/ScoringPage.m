//
//  ScoringPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//


#import "ScoringPage.h"

@interface ScoringPage ()

@end

@implementation ScoringPage
@synthesize tabBar;
@synthesize tableView;
@synthesize shooterPage;
@synthesize settingsPage;
@synthesize setupShootView;
@synthesize storedShootArray;
@synthesize shootShootersObjects;
@synthesize updatedData;
@synthesize spinner;
@synthesize shootToDelete;
@synthesize indexPathToDelete;
@synthesize card;
@synthesize managedContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    
    self.tabBar.delegate = self;
    self.storedShootArray = [[NSMutableArray alloc] init];
    self.shootCoachesArray = [[NSMutableArray alloc] init];
    self.shootShootersArray = [[NSMutableArray alloc] init];
    self.shootShootersObjects = [[NSMutableArray alloc] init];
    self.managedContext = [NSManagedObjectContext MR_contextForCurrentThread];

    //NSLog(@"StoredShoots%@", self.storedShootArray);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.updatedData = [[UpdatedData alloc] init];
    
    
    if([self isNetworkAvailable]){
        
        [self.updatedData getTrapFields];
        NSLog(@"GET TRAP FIELDS SCORE CARD");
    }
  
    
   // self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
    
    // NSLog(@"ViewWillAppear");
 
    
 
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.shooterPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    self.settingsPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Settings"];

    self.setupShootView =
    [storyboard instantiateViewControllerWithIdentifier:@"setupShoot"];
    self.card =
    [storyboard instantiateViewControllerWithIdentifier:@"Card"];
    
    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 1]];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    [spinner performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:YES];
    
    
    [super viewWillAppear:YES];
    
	// Do any additional setup after loading the view.
}

-(void) viewDidLoad {
   // NSLog(@"ViewDidLoad");
    
   
    //self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
    
}

-(void) viewDidAppear:(BOOL)animated{
   
    //self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy]
    
     //NSLog(@"ViewDidAppear");
    
    
    
  /*  self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;
    
    
    if([self isNetworkAvailable] && self.storedShootArray.count == 0){
        
        
        [self.updatedData getShootShooters];
        [self.updatedData getShoots];
        [self.updatedData getShooters];
    
        
        //Try putting a delay on here
        [self.updatedData performSelector:@selector(getCoaches) withObject:nil afterDelay:1.0];
        //[self getDataArrays];
        
        
    }
    else{*/
        
        
        self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
        
        [self getDataArrays];
        
        
    //}
    
    

    
   // self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
   // [self getDataArrays];
    
    //[self.tableView reloadData];

        
        /*[self.updatedData getShootShooters];
        [self.updatedData getShooters];
      
        [self.updatedData getCoaches];*/
        
       // self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
 
              //  [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        
   
    


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) retrieveData{
    
    
    // NSLog(@"HERE");
    if (updatedData.delegateTag == 3) {
         //NSLog(@"HEREInner");
       

       // self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO] mutableCopy];
        
        //[self getDataArrays];
       // [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        [spinner stopAnimating];
    }
   // [self.tableView reloadData];
    
    

}

-(void) getDataArrays {
    
    self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy:@"dateCreated" ascending: NO inContext:self.managedContext] mutableCopy];
   // NSLog(@"StoredShoots%@", self.storedShootArray);
    
    
    //Get Shooters for each shoot
    for(int x = 0; x < self.storedShootArray.count; x++){
        
        StoredShoot *storedShootTemp = [self.storedShootArray objectAtIndex:x];
        
        NSMutableArray *shootShootersTempArray =[[NSMutableArray alloc] init];
        shootShootersTempArray = [[ShootShooters MR_findByAttribute:@"storedShootId" withValue:storedShootTemp.objectId andOrderBy:@"startingPost" ascending:YES inContext:self.managedContext] mutableCopy];
        [self.shootShootersObjects addObject:shootShootersTempArray];
        NSMutableArray *finalShooters = [[NSMutableArray alloc] init];
        
        for(int i = 0; i < shootShootersTempArray.count; i++){
            ShootShooters *shootShooter = [shootShootersTempArray objectAtIndex:i];
            Shooter *shooter = [Shooter MR_findFirstByAttribute:@"objectId" withValue:shootShooter.shooterId inContext:self.managedContext];
        
            [finalShooters addObject:shooter];
        
        }
        [self.shootShootersArray addObject:finalShooters];
    }
    
    
    //Get Coaches
    for(int x = 0; x < self.storedShootArray.count; x++){
        
        StoredShoot *storedShootTemp = [self.storedShootArray objectAtIndex:x];
        
        Coach *tempCoach = [Coach MR_findFirstByAttribute: @"objectId" withValue: storedShootTemp.coachId];
        
        [self.shootCoachesArray performSelector:@selector(addObject:) withObject:tempCoach afterDelay:0.2];
        [self.shootCoachesArray addObject:tempCoach];
        
        
    }
   // NSLog(@"StoredCoaches%@", self.shootCoachesArray);
      // NSLog(@"StoredShooters:%@", self.shootShootersArray);
    [spinner stopAnimating];
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
   // [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [storedShootArray count];
}


- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.tableView setEditing:NO animated:YES];
        
        if([self isNetworkAvailable]){
        
            self.shootToDelete = [self.storedShootArray objectAtIndex:indexPath.row];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Shoot" message:@"This will delete all of the data for this shoot. Are you sure you want to delete it?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", @"DELETE", nil];
            [alert show];
            alert.tag = 1;
            self.indexPathToDelete = indexPath;
            
            
        }
        else{
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to delete a shoot." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        }
    
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
   UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
   
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 100, 25)];
    [dateLabel setFont:[UIFont boldSystemFontOfSize:18]];
    StoredShoot *temp = [self.storedShootArray objectAtIndex:indexPath.row];
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    dateString = [formatter stringFromDate:temp.dateCreated];
    dateLabel.text = dateString;
    
    UILabel *pracOrComp = [[UILabel alloc] initWithFrame:CGRectMake(148 , 6, 170, 25)];
    [pracOrComp setFont:[UIFont boldSystemFontOfSize:16]];
    [pracOrComp setTextColor:[UIColor darkGrayColor]];
    pracOrComp.text = [NSString stringWithFormat:@"Type: %@", temp.typeShoot];
    
    UILabel *trapRange = [[UILabel alloc] initWithFrame:CGRectMake(13, 26, 120, 45)];
    [trapRange setTextColor:[UIColor darkGrayColor]];
    [trapRange setFont:[UIFont boldSystemFontOfSize:16]];
    [trapRange setLineBreakMode:NSLineBreakByWordWrapping];
    
    trapRange.numberOfLines = 2;
    TrapField *trapField;
    NSString *trapFieldString;
    if([temp.trapFieldId isEqualToString:@"NA"]){
        
        trapFieldString = @"No Range Recorded";
    }
    else{
        
        trapField = [TrapField MR_findFirstByAttribute:@"objectId" withValue:temp.trapFieldId];
        trapFieldString = trapField.name;
    }
    [trapRange setText:trapFieldString];
    

    
    UILabel *houseNum = [[UILabel alloc] initWithFrame:CGRectMake(136, 48, 170, 25)];
    [houseNum setFont:[UIFont boldSystemFontOfSize:16]];
    [houseNum setTextColor:[UIColor darkGrayColor]];
    houseNum.text = [NSString stringWithFormat:@"House: #%@", temp.trapHouseNumber];
    
    
    UILabel *handicap = [[UILabel alloc] initWithFrame:CGRectMake(120, 68, 170, 25)];
    [handicap setFont:[UIFont boldSystemFontOfSize:16]];
    [handicap setTextColor:[UIColor darkGrayColor]];
    handicap.text = [NSString stringWithFormat:@"Handicap: %@ yrds", temp.handicap];
    
    
    Coach *tempCoach = [self.shootCoachesArray objectAtIndex:indexPath.row];
    UILabel *coachLabel = [[UILabel alloc] initWithFrame:CGRectMake(135, 27, 185, 25)];
    [coachLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [coachLabel setTextColor:[UIColor darkGrayColor]];
    [coachLabel setText:[NSString stringWithFormat:@"Coach: %@ %@", tempCoach.fName, tempCoach.lName]];
    
    /*UILabel *shootersLabel = [[UILabel alloc] initWithFrame:CGRectMake(116, 60, 100, 25)];
    [shootersLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [shootersLabel setText:[NSString stringWithFormat:@"Shooters"]];*/
    
    UITextView *shootersTextView = [[UITextView alloc] initWithFrame:CGRectMake(40, 93, 240, 125)];
    [shootersTextView setTextAlignment:NSTextAlignmentCenter];
    [shootersTextView setFont:[UIFont systemFontOfSize:16]];
    [shootersTextView setUserInteractionEnabled:NO];
    [shootersTextView.layer setBorderWidth: 1.0f];
    [[shootersTextView layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [shootersTextView.layer setCornerRadius:6.0f];
    [shootersTextView.layer setMasksToBounds:YES];
    [shootersTextView setBackgroundColor:[UIColor lightGrayColor]];
    [shootersTextView setAlpha:0.65];
    NSString *tempShooters = [[NSString alloc] init];
    NSMutableArray *tempShootersArray = [self.shootShootersArray objectAtIndex: indexPath.row];
    NSMutableArray *tempShootShooter = [self.shootShootersObjects objectAtIndex:indexPath.row];
    
   
    
    for(int x = 0; x < tempShootersArray.count; x++){
     
        ShootShooters *shootShooter = [tempShootShooter objectAtIndex:x];
        Shooter *tempShooter = [tempShootersArray objectAtIndex:x];
        tempShooters = [NSString stringWithFormat:@"%@\n%@ %@: %@/%@ Hits", tempShooters, tempShooter.fName, tempShooter.lName, shootShooter.totalHits, shootShooter.totalShots];
    }
    [shootersTextView setText:tempShooters];
    
    [cell addSubview:pracOrComp];
    [cell addSubview:shootersTextView];
    [cell addSubview:handicap];
    [cell addSubview:coachLabel];
    [cell addSubview:trapRange];
    [cell addSubview:houseNum];
    [cell addSubview:dateLabel];

    return cell;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if([item.title isEqualToString:@"Team" ]){
        
        [self presentViewController:shooterPage animated:YES completion:nil];
        
    }
    else if([item.title isEqualToString:@"Settings"]){
        
        [self presentViewController:settingsPage animated:YES completion:nil];
        
    }
    else{
        
        
        
    }
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSLog(@"IndexSelected: %d", indexPath.row);
    [self presentViewController:self.card animated:YES completion:nil];
   
    [self.card showScoreCard:indexPath.row : self.shootShootersArray[indexPath.row] : self.shootShootersObjects[indexPath.row] : storedShootArray[indexPath.row] ];
    


}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}

- (IBAction)setUpNewShoot:(id)sender {
    
    [self presentViewController:setupShootView animated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag > -1) {
        
        if(buttonIndex == 1)
        {
            
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            PFQuery *shooterQuery = [PFQuery queryWithClassName:@"ShootShooters"];
            [shooterQuery whereKey:@"shootID" equalTo:self.shootToDelete.objectId];
            [shooterQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(objects){
                    
                    for(int x = 0; x < objects.count; x ++){
                        
                        [[objects objectAtIndex:x] deleteEventually];
                        
                    }
                    
                    
                    
                }
            }];
            
            
            
            PFQuery *query = [PFQuery queryWithClassName:@"Shoot"];
            [query whereKey:@"appObjectId" equalTo:self.shootToDelete.objectId];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(objects){
                PFObject *tempStored = [objects objectAtIndex:0];
                    [tempStored deleteEventually];
                }
                
            }];
            
            [shootToDelete MR_deleteInContext:localContext];
            
            [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if(success) {
                    //LOOK AT WHY IT IS SHOWING UP THE WRONG THING WHEN A SHOOT OS DELETED
                   // NSLog(@"Delete ShootLocal: %d", indexPathToDelete.row);
                   
                    
                }
            }];
            
            [self.storedShootArray removeObjectAtIndex:indexPathToDelete.row];
            self.storedShootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        
        }
        
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }
    else{
        
        [self.tableView setEditing:NO animated:YES];
        
    }
    
    
    
}

@end
