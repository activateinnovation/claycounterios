//
//  ScoringShootPage.h
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetupShootView.h"
#import "Shooter.h"
#import "Coach.h"
#import "CustomShootCell.h"
#import "ShootObjectClass.h"
#import "ScoringPage.h"
#import "SetupShootView.h"
#import "CurrentShoot.h"
#import "ShooterShootObject.h"
#import "StoredShoot.h"
#import "ShootShooters.h"

#import "MSCMoreOptionTableViewCell.h"

@class SetupShootView;
@class ShootObjectClass;
@class ScoringPage;
@class Coach;
@class CustomShootCell;


@interface ScoringShootPage : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, MSCMoreOptionTableViewCellDelegate>
@property (strong ,nonatomic) CurrentShoot *currentShoot;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *testButton;
@property (strong, nonatomic) NSString *tempShootId;
- (IBAction)undoLastAction:(id)sender;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray *startingPosts;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *undoOutlet;
@property (strong, nonatomic) IBOutlet UIButton *undoButton;
@property (strong, nonatomic) Coach *coach;
@property (strong, nonatomic) UIAlertView *endAlert;
@property (strong, nonatomic) IBOutlet UIButton *leftPlusButton;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *straightButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIButton *rightPlusButton;
@property (strong, nonatomic) IBOutlet UIButton *hitButton;
@property (strong, nonatomic) NSString *coachId;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property int counter;
@property int shotCounter;
-(void) goToCurrentCell;
@property (strong, nonatomic) ScoringPage *scoringPage;
@property (strong, nonatomic) ShootObjectClass *shootObject;
@property (strong, nonatomic) SetupShootView *setUpShoot;
@property (strong, nonatomic) NSMutableArray *shooterShootArray;
- (IBAction)cancelShoot:(id)sender;
-(IBAction)scoringButtonCLicked:(id)sender;
//-(void)textFieldClicked: (CustomShootCell *)cell : (UITextField *)textField : (NSMutableArray *)shootShooter;
@property int shotsCounter;
@property int shooterIndex;
@property int roundNumber;
@property int previousShooterIndex;
@property int modifyScoreTag;
@property (strong, nonatomic) NSMutableArray *startingPostNumbers;

@end
