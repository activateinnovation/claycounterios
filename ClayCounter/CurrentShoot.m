//
//  CurrentShoot.m
//  ClayCounter
//
//  Created by Test on 2/6/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "CurrentShoot.h"


@implementation CurrentShoot

@dynamic coach;
@dynamic shooters;
@dynamic totalShots;
@dynamic shotsAtPost;
@dynamic pracOrComp;
@dynamic startingPost;
@dynamic trapFieldId;
@dynamic trapHouseNumber;
@dynamic handicap;
@end
