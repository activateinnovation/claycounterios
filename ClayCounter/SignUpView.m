//
//  SignUpView.m
//  ClayCounter
//
//  Created by Test on 1/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SignUpView.h"

@interface SignUpView ()

@end

@implementation SignUpView
@synthesize usernameTextField;
@synthesize emailTextField;
@synthesize orgNameTextField;
@synthesize homeTrapRangeTextField;
@synthesize passwordTextField;
@synthesize repeatPasswordTextField;
@synthesize shooterPageView;
@synthesize passwordLabel;
@synthesize teamInfoLabel;
@synthesize headCoachLabel;
@synthesize headCoachFName;
@synthesize headCoachLName;
@synthesize headCoachPhoneNum;
@synthesize counter;
@synthesize NextSelected;
@synthesize tempSignUpData;
@synthesize user;
@synthesize coach;
@synthesize loginView;
@synthesize rightHandedLabel;
@synthesize leftRightSwitch;
@synthesize leftHandedLabel;
@synthesize decriptionTextView;
@synthesize updateData;
@synthesize pickerView;
@synthesize trapFieldsArray;
@synthesize trapFieldId;
@synthesize toolBar;
@synthesize menu;
@synthesize managedObjectContext;
@synthesize checkmarkImageView;
@synthesize spinner;
@synthesize checkmarkImageView2;
@synthesize trapFieldSelected;
@synthesize headCoachEmail;


-(void) viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:YES];
    self.trapFieldId = [[NSString alloc] init];
    self.tempSignUpData = [[NSMutableArray alloc] init];
    self.updateData = [[UpdatedData alloc] init];
    self.trapFieldsArray = [[NSMutableArray alloc] init];
    self.trapFieldSelected = [PFObject objectWithClassName: @"TrapField"];
    self.trapFieldsArray = [self getTrapFields];

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:tap];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.shooterPageView =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    
    UIStoryboard* storyboard1 = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.loginView =
    [storyboard1 instantiateViewControllerWithIdentifier:@"LoginView"];

   // self.checkmarkImageView = [[UIImageView alloc] init];
    
    
    self.counter = 0;
    
    [self.usernameTextField becomeFirstResponder];
    //self.errorImageView.hidden = YES;
    //self.checkmarkImageView.hidden = YES;
    
   //Show the team information section
    self.teamInfoLabel.hidden = NO;
    self.usernameTextField.hidden = NO;
    self.orgNameTextField.hidden = NO;
    self.homeTrapRangeTextField.hidden = NO;
    self.emailTextField.hidden = NO;

    self.headCoachFName.hidden = YES;
    self.headCoachLabel.hidden = YES;
    self.headCoachLName.hidden = YES;
    self.headCoachPhoneNum.hidden = YES;
    self.headCoachEmail.hidden = YES;
    self.rightHandedLabel.hidden = YES;
    self.leftHandedLabel.hidden = YES;
    self.leftRightSwitch.hidden = YES;
    self.decriptionTextView.hidden = YES;
    
    self.passwordLabel.hidden = YES;
    self.passwordTextField.hidden = YES;
    self.repeatPasswordTextField.hidden = YES;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, 320, 216)];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
 
    [self.homeTrapRangeTextField setInputView: self.pickerView];
    [self.pickerView reloadAllComponents];
    
   // self.pickerView.hidden = YES;

}

-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
    self.counter = 0;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelSignUp:(id)sender {

    [self presentViewController: self.loginView
                       animated:YES
                     completion:nil];
    
}

-(IBAction)keyboardNext:(id)sender{
    
    if(sender == usernameTextField){
        
        [self checkUsername];
        [usernameTextField resignFirstResponder];
        [emailTextField becomeFirstResponder];
        
    }
    else if (sender == emailTextField){
        [self checkEmail];
        [emailTextField resignFirstResponder];
        [orgNameTextField becomeFirstResponder];
        
    }
    else if(sender == orgNameTextField){
        
        [orgNameTextField resignFirstResponder];
        [homeTrapRangeTextField becomeFirstResponder];
        //Put in the picker view here when homeTrapField becomes first responer
        
    }
    else if(sender == passwordTextField) {
        
        [passwordTextField resignFirstResponder];
        [repeatPasswordTextField becomeFirstResponder];
        
    }
    else if(sender == headCoachFName){
        
        [headCoachFName resignFirstResponder];
        [headCoachLName becomeFirstResponder];
        
    }
    else if (sender == headCoachLName){
        
        [headCoachLName resignFirstResponder];
        [headCoachPhoneNum becomeFirstResponder];
        
    }
    else if(sender == headCoachPhoneNum){
        [headCoachPhoneNum resignFirstResponder];
        [headCoachEmail becomeFirstResponder];
        
    }
    else{
        
    }
    
    
}

-(NSMutableArray *) getTrapFields{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            
            [tempArray addObjectsFromArray:objects];
            NSLog(@"%@", tempArray);
        }
        else{
            
            NSLog(@"Error getting trap fields");
        }
        
    }];
    
    return tempArray;
    
}

/*-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}*/
-(void) doneOnPasswordField{
    
    [self initiateSignUpProcess];
    
}


-(IBAction)keyboardDone:(id)sender{
    
    if(sender == homeTrapRangeTextField){
        [homeTrapRangeTextField resignFirstResponder];
    }
    else if (sender == repeatPasswordTextField){
        [repeatPasswordTextField resignFirstResponder];
        [self doneOnPasswordField];
        
    }
    else if(sender == self.headCoachEmail){
        
        [self.headCoachEmail resignFirstResponder];
    }
    else{
        
    }
    
}



-(void) tapGesture {
    
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.orgNameTextField resignFirstResponder];
    [self.homeTrapRangeTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.repeatPasswordTextField resignFirstResponder];
    [self.headCoachPhoneNum resignFirstResponder];
    [self.headCoachLName resignFirstResponder];
    [self.headCoachFName resignFirstResponder];
    [self.headCoachEmail resignFirstResponder];
}




- (IBAction)nextSelectedAction:(id)sender {
    
    if(self.counter == 0){
        
      //Set Parse User Data USERNAME, EMAIL, ORGANIZATION, and TRAP FIELD
        if(self.emailTextField.text.length == 0 || self.orgNameTextField.text.length == 0 || self.homeTrapRangeTextField.text.length == 0 || self.usernameTextField.text.length == 0 || self.checkmarkImageView.image == [UIImage imageNamed:@"error.png"] || checkmarkImageView2.image == [UIImage imageNamed:@"error.png"] ){
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        else {
           
            
            [self.tempSignUpData addObject:usernameTextField.text];
            [self.tempSignUpData addObject:emailTextField.text];
            [self.tempSignUpData addObject:orgNameTextField.text];
            [self.tempSignUpData addObject: homeTrapRangeTextField.text];
            
            //Show head coach info
            self.teamInfoLabel.hidden = YES;
            self.usernameTextField.hidden = YES;
            self.orgNameTextField.hidden = YES;
            self.homeTrapRangeTextField.hidden = YES;
            self.emailTextField.hidden = YES;
            
            self.headCoachFName.hidden = NO;
            self.headCoachLabel.hidden = NO;
            self.headCoachLName.hidden = NO;
            self.headCoachPhoneNum.hidden = NO;
             self.headCoachEmail.hidden = NO;
            self.rightHandedLabel.hidden = NO;
            self.leftHandedLabel.hidden = NO;
            self.leftRightSwitch.hidden = NO;
            self.decriptionTextView.hidden = NO;
            
            
            self.passwordLabel.hidden = YES;
            self.passwordTextField.hidden = YES;
            self.repeatPasswordTextField.hidden = YES;
            
            self.checkmarkImageView.hidden = YES;
            self.checkmarkImageView2.hidden = YES;
 
            
            [self.homeTrapRangeTextField resignFirstResponder];
            [self.headCoachFName becomeFirstResponder];
             self.counter++;
        }
        
    }
    else if (self.counter == 1){
    
        
        //Add parse dacta to array COACH FNAME, COACH LNAME, COACH Email
        if((self.headCoachFName.text.length == 0) || (self.headCoachLName.text.length == 0) || (self.headCoachEmail.text.length == 0) ){
       
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        else{
            
            [self.tempSignUpData addObject:headCoachFName.text];
            [self.tempSignUpData addObject:headCoachLName.text];
            [self.tempSignUpData addObject:headCoachPhoneNum.text];
            [self.tempSignUpData addObject:headCoachEmail.text];
            
            if([self.leftRightSwitch isOn]){
                
                NSString *leftRight = @"Right";
                [self.tempSignUpData addObject:leftRight];
                
            }
            else{
                
                NSString *leftRight = @"Left";
                [self.tempSignUpData addObject:leftRight];
            }
            
            //Show the password fields
            
            [self.NextSelected setTitle:@"Done" forState:UIControlStateNormal];
            
            self.teamInfoLabel.hidden = YES;
            self.usernameTextField.hidden = YES;
            self.orgNameTextField.hidden = YES;
            self.homeTrapRangeTextField.hidden = YES;
            self.emailTextField.hidden = YES;
            
            self.headCoachFName.hidden = YES;
            self.headCoachLabel.hidden = YES;
            self.headCoachLName.hidden = YES;
            self.headCoachPhoneNum.hidden = YES;
             self.headCoachEmail.hidden = YES;
            self.rightHandedLabel.hidden = YES;
            self.leftHandedLabel.hidden = YES;
            self.leftRightSwitch.hidden = YES;
            self.decriptionTextView.hidden = YES;
            
            
            self.passwordLabel.hidden = NO;
            self.passwordTextField.hidden = NO;
            self.repeatPasswordTextField.hidden = NO;
            
            [self.headCoachEmail resignFirstResponder];
            [self.passwordTextField becomeFirstResponder];
              self.counter++;
        }
        //NO(false) means LEFT and YES(true) means RIGHT
        
       }
    else if (self.counter == 2){
        
        //Need to check passwords for matching here
        [self initiateSignUpProcess];
       /* if (self.passwordTextField.text.length < 6 || self.repeatPasswordTextField.text.length < 6) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Password needs to be at least 6 characters long." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            

            
        }else{
            
            if ([self.passwordTextField.text isEqualToString:self.repeatPasswordTextField.text] ) {
            
                 //Add data to parse array PASSWORD
                [self.tempSignUpData addObject:passwordTextField.text];
            
                NSLog(@"%@", self.tempSignUpData[7]);
                
                //Add the user to parse, check internet connect
                user = [PFUser user];
                user.username = self.tempSignUpData[0];
                user.password = self.tempSignUpData[8];
                user.email = self.tempSignUpData[1];
                user[@"Organization"] = self.tempSignUpData[2];
                user[@"homeField"] = @"NV76e1lP5y";
                
                
                coach = [PFObject objectWithClassName:@"Coach"];
                coach[@"fName"] = self.tempSignUpData[4];
                coach[@"lName"] = self.tempSignUpData[5];
                coach[@"phone"] = self.tempSignUpData[6];
                coach[@"leftRightHanded"] = self.tempSignUpData[7];
                
                
                [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    if (success) {
                        
                        NSLog(@"New Shooter Added to Local Storage");
                        
                    }
                }];
                
                
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (!error) {
                        
                        
                        [self setUpUser];
                        
                    } else {
                        NSString *errorString = [error userInfo][@"error"];
                        NSLog(@"%@", errorString);
                    }
                }];
                
                
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Please make sure both passwords match." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
                
                
            }
        }
      */
        
    }
}




-(void) initiateSignUpProcess{
    
    if (self.passwordTextField.text.length < 6 || self.repeatPasswordTextField.text.length < 6) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Password needs to be at least 6 characters long." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
        
        
    }else{
        
        if ([self.passwordTextField.text isEqualToString:self.repeatPasswordTextField.text] ) {
            [spinner startAnimating];
            //Add data to parse array PASSWORD
            [self.tempSignUpData addObject:passwordTextField.text];
            
           // NSLog(@"%@", self.tempSignUpData[7]);
            
             NSLog(@"TrapField: %@, %@", self.trapFieldSelected[@"Name"], self.trapFieldSelected.objectId);
            //Add the user to parse, check internet connect
            user = [PFUser user];
            user.username = self.tempSignUpData[0];
            user.password = self.tempSignUpData[9];
            user.email = self.tempSignUpData[1];
            user[@"Organization"] = self.tempSignUpData[2];
            user[@"homeField"] = self.trapFieldSelected.objectId;
            user[@"homeFieldName"] = self.trapFieldSelected[@"Name"];
            
            
            
            coach = [PFObject objectWithClassName:@"Coach"];
            coach[@"fName"] = self.tempSignUpData[4];
            coach[@"lName"] = self.tempSignUpData[5];
            coach[@"phone"] = self.tempSignUpData[6];
            coach[@"email"] = self.tempSignUpData[7];
            coach[@"leftRightHanded"] = self.tempSignUpData[8];
            
            
            [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if (success) {
                    
                    NSLog(@"New Shooter Added to Local Storage");
                    
                }
            }];
            
            
            [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    
                    
                    [self setUpUser];
                    
                } else {
                    NSString *errorString = [error userInfo][@"error"];
                    NSLog(@"%@", errorString);
                }
            }];
            
            
        }
        else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Please make sure both passwords match." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        }
    }
    
    
}






-(void) setUpUser{
    
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];

     NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", [self.tempSignUpData objectAtIndex:4], [self.tempSignUpData objectAtIndex:5]]];
    
    [PFUser logInWithUsernameInBackground: self.usernameTextField.text password: self.passwordTextField.text block:^(PFUser *userTemp, NSError *error) {
        if (userTemp) {
            
           
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stayLoggedIn"];
            
            
            Coach *coachMR = [Coach MR_createInContext:self.managedObjectContext];
            coachMR.fName = [self.tempSignUpData objectAtIndex:4];
            coachMR.lName = [self.tempSignUpData objectAtIndex:5];
            coachMR.phone = [self.tempSignUpData objectAtIndex:6];
            coachMR.email = [self.tempSignUpData objectAtIndex:7];
            coachMR.leftRightHanded = [self.tempSignUpData objectAtIndex:8];
            coachMR.userId = [PFUser currentUser].objectId;
            // int x =[Coach MR_countOfEntities];
            coachMR.objectId = tempHashId;
            
            [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if (success) {
                    
                    NSLog(@"New Coach Added to Local Storage");
                    
                }
            }];
          
            userTemp = [PFUser currentUser];
            coach[@"userId"] = userTemp.objectId;
            coach[@"appObjectId"] = tempHashId;
            [coach saveInBackground];
            
            //This checks for shooters that can be loaded from parse and put into persistent memory so the app can be used in offline mode
            [self.updateData getShooters];
            [self.spinner stopAnimating];
            [self presentWelcomeAlert];
            [self presentViewController:shooterPageView animated:YES completion:nil];
            
            
        } else {
        
            
        }
    }];
}



-(void) presentWelcomeAlert{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Welcome!" message:[NSString stringWithFormat:@"Welcome to iTrap, %@! We suggest that you begin by adding your team shooters by clicking the bottom left tab.", [PFUser currentUser][@"username"]] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
   
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    
}

-(void) checkEmail {
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"email" equalTo:self.emailTextField.text]; // find all the women
    NSMutableArray *users = [[query findObjects] mutableCopy];
    
    // NSLog(@"%d", users.count);
    
    if (self.emailTextField.text.length == 0 ) {
        
        [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"error.png"]];
        
    }
    else{
        if(users.count <= 0){
            
            [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"checkmark.png"]];
        }
        else{
            self.emailTextField.text = @"";
            self.emailTextField.placeholder = @"Email already in use";
            [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"error.png"]];
        }
        
    }

    
    
    
    
}


-(void) checkUsername {

    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:self.usernameTextField.text]; // find all the women
    NSMutableArray *users = [[query findObjects] mutableCopy];

   // NSLog(@"%d", users.count);
   
    if (self.usernameTextField.text.length == 0 ) {
        
        [self.checkmarkImageView setImage:[UIImage imageNamed:@"error.png"]];
        
    }
    else{
            if(users.count <= 0){
        
                [self.checkmarkImageView setImage:[UIImage imageNamed:@"checkmark.png"]];
            }
            else{
                self.usernameTextField.text = @"";
                self.usernameTextField.placeholder = @"Username already taken";
                [self.checkmarkImageView setImage:[UIImage imageNamed:@"error.png"]];
            }

    }
    
}

#pragma mark PICKERVIEW DELEGATE

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}



// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.trapFieldsArray count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = [self.trapFieldsArray objectAtIndex:row][@"Name"];
    
    return title;
}
/*- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return (80.0);
}*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.trapFieldSelected = [self.trapFieldsArray objectAtIndex:row];
   
    self.homeTrapRangeTextField.text = [self.trapFieldsArray objectAtIndex:row][@"Name"];
        NSLog(@"Selected");
    
}

/*- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont systemFontOfSize:10]];
        [tView sizeToFit];
        // Setup label properties - frame, font, colors etc
        
    }
    // Fill the label text here
    tView.text = [self.trapFieldsArray objectAtIndex:row][@"Name"];
    
    return tView;
}*/


- (IBAction)teamTrapRangeClicked:(id)sender {
    
    self.trapFieldSelected = [self.trapFieldsArray objectAtIndex:0];
    
    self.homeTrapRangeTextField.text = self.trapFieldSelected[@"Name"];
    
}

- (IBAction)usernameCheckDidEnd:(id)sender {
    
    
    [self checkUsername];
}
- (IBAction)checkEmailDone:(id)sender {
    
    [self checkEmail];

}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

@end
