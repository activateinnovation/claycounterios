//
//  ShooterShootObject.h
//  ClayCounter
//
//  Created by ActivateInnovation on 2/9/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shooter.h"

@class Shooter;

@interface ShooterShootObject : NSObject

@property (strong, nonatomic) Shooter *shooter;
@property int postNumber;
@property int roundNumber;
@property int shotCounter;
@property int hitNumber;
@property int hitsInRound;
@property (strong, nonatomic) NSMutableArray *scoreArray;
@property int shotsFiredInRound;
-(ShooterShootObject *) initWithItems: (Shooter *)shooterTemp : (int)postNumberTemp : (int) shotCounterTemp : (int) hitNumberTemp :(NSMutableArray *)scoreArrayTemp : (int)shotsFiredInRoundTemp :(int) hitsInShootTemp;


@end
