//
//  LoginView.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "LoginView.h"

@interface LoginView ()

@end

@implementation LoginView

@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize shooterPage;
@synthesize spinner;
@synthesize signUp;
@synthesize userDefaults;
@synthesize updatedData;
@synthesize scoringPage;
@synthesize counter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//Sets up the loading spinner and centers it on the page.
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.stayLoggedInSwitch.on = YES;
    self.updatedData = [[UpdatedData alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.counter = 0;
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    BOOL loggedIn = [userDefaults boolForKey:@"stayLoggedIn"];
    
    if(loggedIn == YES){
        
        if([PFUser currentUser]){
        
        [spinner startAnimating];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        self.scoringPage =
        [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
        [self presentViewController:scoringPage animated: YES completion:^{
            
            [spinner stopAnimating];
        
        }];
        }
        else{
            
            
        }
    }
    else{
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginAction:(id)sender {
    
    [self login];
}


-(void) login{
    
    //NEED TO CHECK THIS OUT WITH LOGGING IN WITHOUT INTERNET, MAKE STAY LOGGED IN SWITCH 1/17/14
    
    [spinner startAnimating];
    [PFUser logInWithUsernameInBackground: self.usernameTextField.text password: self.passwordTextField.text block:^(PFUser *user, NSError *error) {
        if (user) {
            
            if ([self.stayLoggedInSwitch isOn]) {
                
                [userDefaults setBool:YES forKey:@"stayLoggedIn"];
            }
            else{
                
                [userDefaults setBool:NO forKey:@"stayLoggedIn"];
            }

            
            //[PFUser currentUser] = user;
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            self.scoringPage =
            [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
            
            //This checks for shooters that can be loaded from parse and put into persistent memory so the app can be used in offline mode
           // [updatedData getShooters];
            //[updatedData getCoaches];
            self.updatedData = [[UpdatedData alloc] init];
            self.updatedData.delegate = self;
            
            
            if([self isNetworkAvailable]){
                
                [self.updatedData performSelectorOnMainThread:@selector(getShoots) withObject:nil waitUntilDone:YES];
                [self.updatedData performSelector:@selector(getShootShooters) withObject:nil afterDelay:0.2];
                [self.updatedData performSelector:@selector(getShooters) withObject:nil afterDelay:0.2];
                [self.updatedData performSelector:@selector(getCoaches) withObject:nil afterDelay:0.2];
                [self.updatedData performSelector:@selector(getTrapFields) withObject:nil afterDelay:0.2];
                //[self.updatedData getShooters];
                //[self.updatedData getCoaches];
                //[self getDataArrays];
                
            }

            
            
            
            
        } else {
            
            [spinner stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account" message:@"Username or password is incorrect." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
      
        }
    }];
}


-(void)retrieveData {
    
    counter++;
    NSLog(@"Counter: %d", counter);

    if(counter == 2){
        
        NSLog(@"Too many");
        [self performSelector:@selector(presentScoringPage) withObject:nil afterDelay:0.8];
    }
    
    
}

-(void) presentScoringPage {
    
    [spinner stopAnimating];
    [self presentViewController:self.scoringPage animated:YES completion:nil];
}


-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
    counter = 0;
    
}

- (IBAction)nextOnKeyboard:(id)sender {
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField becomeFirstResponder];
}

- (IBAction)dismissKeyboard:(id)sender {
    
    [self resignFirstResponder];
    [self login];
}

- (IBAction)tapDismiss:(id)sender {
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}


- (IBAction)signUp:(id)sender {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.signUp =
    [storyboard instantiateViewControllerWithIdentifier:@"SignUp"];
    
    [self presentViewController:signUp animated:YES completion:nil];

}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!APPDelegate\n");
        return YES;
    }
}

@end
