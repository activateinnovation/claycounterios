//
//  SettingsPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SettingsPage.h"

@interface SettingsPage ()

@end

@implementation SettingsPage

@synthesize scoringPage;
@synthesize shootersPage;
@synthesize userDefaults;
@synthesize updatedData;
@synthesize container;
@synthesize addCoachView;
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNum;
@synthesize emailAddress;
@synthesize managedObjectContext;
@synthesize leftRightSwitch;
@synthesize spinner;
@synthesize cancelButtonOutlet;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.cancelButtonOutlet.enabled = NO;
    self.coachesDataArray = [[NSMutableArray alloc] init];
   /* self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;*/
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height * 0.75)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    [spinner startAnimating];

    //[updatedData getCoaches];

    self.coachesDataArray = [[Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
    [self.tableView reloadData];
    [spinner stopAnimating];
    
    
    if(self.view.frame.size.width > 320){
        
        self.leftRightSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.15, 200, 40, 35)];
    }else{
        self.leftRightSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(135, 200, 40, 35)];
    }
    //This fills all of the labels in the settings page with user data
    self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];
    self.addCoachView = [[UIView alloc] initWithFrame:self.container.bounds];
    self.addCoachView.clearsContextBeforeDrawing = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.addCoachView addGestureRecognizer:tap];
    [self setUpView];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
  self.cancelButtonOutlet.enabled = NO;
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.tabBar.delegate = self;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.shootersPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    UIStoryboard* storyboard2 = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.scoringPage =
    [storyboard2 instantiateViewControllerWithIdentifier:@"Score Card"];
    
    UIStoryboard* storyboard3 = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.loginView =
    [storyboard3 instantiateViewControllerWithIdentifier:@"LoginView"];
    
    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 2]];
	// Do any additional setup after loading the view.
   
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)retrieveData{
    
    NSLog(@"coachesPage: %@", updatedData.coachesArray);
    self.coachesDataArray = updatedData.coachesArray;
    [self.tableView reloadData];
    [spinner stopAnimating];
}*/

-(void) setUpView{
    
    self.usernameLabel.text = [PFUser currentUser][@"username"];
    self.organizationLabel.text = [PFUser currentUser][@"Organization"];
    self.trapFieldLabel.text = [PFUser currentUser][@"homeFieldName"];
    self.numOfShootersLabel.text = [NSString stringWithFormat:@"%d Team Shooters", [[Shooter MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] count]];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.coachesDataArray count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
 
    Coach *tempCoach = [self.coachesDataArray objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", tempCoach.fName, tempCoach.lName];
    
    
    return cell;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if([item.title isEqualToString:@"Team" ]){
        
        [self presentViewController:shootersPage animated:YES completion:nil];
        
    }
    else if([item.title isEqualToString:@"Score Card"]){
        
         [self presentViewController:scoringPage animated:YES completion:nil];
        
    }
    else{
        
        
        
    }
    
    
     
}


-(void) setUpAddCoachView{
    
    [self.addCoachView setBackgroundColor:[UIColor lightGrayColor]];
 
    [self.leftRightSwitch setOn:YES];
    [self.leftRightSwitch setOnTintColor:[UIColor orangeColor]];
    
     self.cancelButtonOutlet.enabled = YES;
    
    UITextView *infoText;
    UILabel *leftHanded;
    UILabel *rightHanded;
    UIButton *addCoach;
    if(self.view.frame.size.width > 320){
        
         infoText = [[UITextView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.7, 230, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.15, 204, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/1.8, 204, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 30, 280, 30)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 70, 280, 30)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 110, 280, 30)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 150, 280, 30)];
        addCoach = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 320, 120, 50)];

    }
    else{
        
         infoText = [[UITextView alloc] initWithFrame:CGRectMake(60, 230, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(30, 204, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(200, 204, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(20, 30, 280, 30)];
         lastName = [[UITextField alloc] initWithFrame:CGRectMake(20, 70, 280, 30)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(20, 110, 280, 30)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(20, 150, 280, 30)];
        addCoach = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.1, 320, 120, 50)];
        
        
        
    }
   
    infoText.backgroundColor = [UIColor clearColor];
    infoText.text = @"This information is used to adjust the app to best fit you!";
    [infoText setFont:[UIFont systemFontOfSize:14]];
    [infoText setTextAlignment:NSTextAlignmentCenter];
    self.logOutOutlet.enabled = NO;
    //self.editAccountOutlet.enabled = NO;
    self.navBar.topItem.title = @"Add Coach";
    // [self.addShooterView setAlpha:0.9];
    
    leftHanded.text = @"Left Handed";
    [leftHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
   
    rightHanded.text = @"Right Handed";
    [rightHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setReturnKeyType:UIReturnKeyNext];
    [firstName setBackgroundColor:[UIColor whiteColor]];
    [firstName setBorderStyle:UITextBorderStyleRoundedRect];
    [firstName setPlaceholder:@"First Name"];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackgroundColor:[UIColor whiteColor]];
    [lastName setBorderStyle:UITextBorderStyleRoundedRect];
    [lastName setPlaceholder:@"Last Name"];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
   
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor whiteColor]];
    [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setPlaceholder:@"Phone Number (Optional)"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
   
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [emailAddress setReturnKeyType:UIReturnKeyDone];
    [emailAddress setBackgroundColor:[UIColor whiteColor]];
    [emailAddress setBorderStyle:UITextBorderStyleRoundedRect];
    [emailAddress setPlaceholder:@"Email Address (Required)"];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

 
    
    
    [addCoach setTitle:@"Add Coach" forState:UIControlStateNormal];
    [addCoach setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addCoach.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [addCoach.titleLabel sizeToFit];
    [addCoach addTarget:self action:@selector(addCoachToData) forControlEvents:UIControlEventTouchUpInside];
    [addCoach setEnabled:true];
    
    /*UIButton *cancelAddCoach = [[UIButton alloc] initWithFrame:CGRectMake(30, 400, 100, 50)];
    [cancelAddCoach setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelAddCoach setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelAddCoach.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [cancelAddCoach.titleLabel sizeToFit];
    [cancelAddCoach addTarget:self action:@selector(cancelAddCoach) forControlEvents:UIControlEventTouchUpInside];
    [cancelAddCoach setEnabled:true];*/
    
    

    [self.addCoachView addSubview:infoText];
    [self.addCoachView addSubview:firstName];
    [self.addCoachView addSubview:lastName];
    [self.addCoachView addSubview:phoneNum];
    [self.addCoachView addSubview:emailAddress];
    [self.addCoachView addSubview:addCoach];
    //[self.addCoachView addSubview:cancelAddCoach];
    [self.addCoachView addSubview:leftRightSwitch];
    [self.addCoachView addSubview:leftHanded];
    [self.addCoachView addSubview:rightHanded];
    
}

-(void) nextOnKeyboard : (id) sender {
    
    if(sender == self.firstName)
    {
        [self.firstName resignFirstResponder];
        [self.lastName becomeFirstResponder];
    }
    else if(sender == self.lastName){
        
        [self.lastName resignFirstResponder];
        [self.phoneNum becomeFirstResponder];
    }
    else if (sender == self.phoneNum){
        
        [self.phoneNum resignFirstResponder];
        [self.emailAddress becomeFirstResponder];
    }
    else if(sender == self.emailAddress){
        [self.emailAddress resignFirstResponder];
        
    }

    
}


-(void) addCoachToData {
   // NSLog(@"Add Coach");
    
    
    if(self.firstName.text.length < 2 || self.lastName.text.length < 2 || self.emailAddress.text.length < 4){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled in." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
    NSString *leftRight = [[NSString alloc] init];
    if([self.leftRightSwitch isOn]){
        leftRight = @"Right";
    
    }else{
        
        leftRight = @"Left";
    }
    
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    Coach *coach = [Coach MR_createInContext:self.managedObjectContext];
    coach.fName = self.firstName.text;
    coach.lName = self.lastName.text;
    coach.phone = self.phoneNum.text;
    coach.email = self.emailAddress.text;
    coach.leftRightHanded = leftRight;
    coach.userId = [PFUser currentUser].objectId;
    //int x = [Shooter MR_countOfEntities];
    coach.objectId = tempHashId;//[NSNumber numberWithInt:(x + 1)];
    
    [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            
            PFObject *coachObject = [PFObject objectWithClassName:@"Coach"];
            coachObject[@"appObjectId"] = tempHashId;
            coachObject[@"fName"] = self.firstName.text;
            coachObject[@"lName"] = self.lastName.text;
            coachObject[@"phone"] = self.phoneNum.text;
            coachObject[@"leftRightHanded"] = leftRight;
            coachObject[@"email"] = self.emailAddress.text;
            coachObject[@"userId"] = [PFUser currentUser].objectId;
            
            [coachObject saveEventually:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    
                    
                    NSLog(@"SavedCoachToParse");
                    
                    
                }
            }];
            
            
        }
    }];
    
    [self.coachesDataArray removeAllObjects];
    
    self.coachesDataArray = [[Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
    
    [self.tableView reloadData];
    
    for(UIView *view in self.addCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addCoachView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addCoachView removeFromSuperview];
    self.logOutOutlet.enabled = YES;
   // self.editAccountOutlet.enabled = YES;
    self.navBar.topItem.title = @"Settings";
    }//else
    
    
    
    
}

//called on add coachview to dismiss keybord from all view
-(void) tapGesture{
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    
}

- (IBAction)logOut:(id)sender {
    
    [PFUser logOut];
    
    [userDefaults setBool:NO forKey:@"stayLoggedIn"];
    
    [self presentViewController: self.loginView animated:YES completion:nil];
    
}
- (IBAction)editAccount:(id)sender {
    
    
    
}
- (IBAction)addCoach:(id)sender {
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpAddCoachView];
                        [self.container addSubview: self.addCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];
    
}


-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

- (IBAction)cancelAddCoach:(id)sender {
    
   // NSLog(@"Cancel Add Coach");
    
    for(UIView *view in self.addCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addCoachView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addCoachView removeFromSuperview];
    self.logOutOutlet.enabled = YES;
    self.cancelButtonOutlet.enabled = NO;
    self.navBar.topItem.title = @"Settings";
    
    
}


//THIS SECTION OF CODE SETS UP THE NECESSARY METHODS FOR DELETEING A COACH

/*- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        if([self isNetworkAvailable]){
            
            self.coachToDelete = [self.coachesDataArray objectAtIndex:indexPath.row];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Coach" message:@"This will delete all of the data for this shoot. Are you sure you want to delete it?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"DELETE", nil];
            [alert show];
            alert.tag = indexPath.row;
            
            
        }
        else{
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to delete a coach." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        }
        
    }
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}
*/


@end
