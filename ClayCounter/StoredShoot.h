//
//  StoredShoot.h
//  ClayCounter
//
//  Created by Test on 2/27/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StoredShoot : NSManagedObject

@property (nonatomic, retain) NSString * coachId;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * typeShoot;
@property (nonatomic, retain) NSNumber * totalShots;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSNumber * shotsPerRotation;
@property (nonatomic , retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString *trapFieldId;
@property (nonatomic, retain) NSNumber *trapHouseNumber;
@property (nonatomic, retain) NSNumber *handicap;
@end
