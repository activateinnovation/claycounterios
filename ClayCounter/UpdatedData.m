//
//  UpdatedData.m
//  ClayCounter
//
//  Created by Test on 1/18/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "UpdatedData.h"

@implementation UpdatedData
@synthesize managedObjectContext;
@synthesize shootersArray;
@synthesize numOfParseShooters;
@synthesize numOfSavedShooters;
@synthesize numeOfSavedCoaches;
@synthesize numOfParseCoaches;
@synthesize coachesArray;
@synthesize shootArray;
@synthesize numOfParseShoots;
@synthesize numOfSavedShoots;
@synthesize numOfParseShootShooters;
@synthesize numOfSavedShootShooters;
@synthesize trapFieldsArray;

-(BOOL) isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}




-(void) getShooters {

    BOOL connection = [self isNetworkAvailable];
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    
    if(connection){
        
       // NSLog(@"WE R HEREE");
        
        numOfSavedShooters = 0;
        numOfParseShooters = 0;
        NSMutableArray *parseShooters = [[NSMutableArray alloc] init];
        shootersArray = [[NSMutableArray alloc] init];
      
         self.shootersArray = [[Shooter MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
        [query whereKey:@"userId" equalTo: [PFUser currentUser].objectId];
        [query orderByDescending: @"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
           // NSLog(@"Successfully retrieved %lu shooters.", (unsigned long)objects.count);
                [parseShooters addObjectsFromArray:objects];
                // Do something with the found objects
                
                 self.numOfSavedShooters = [shootersArray count];
                 self.numOfParseShooters = [parseShooters count];
                
               // NSLog(@"%@", parseShooters);
                
               
                
                
                if(self.numOfSavedShooters < self.numOfParseShooters){
                    
                    int numberToUpdate = (self.numOfParseShooters - self.numOfSavedShooters);
                    
                    for(int x = 0; x < numberToUpdate; x++){
                        
                        
                        Shooter *shooter = [Shooter MR_createInContext:self.managedObjectContext];
                        
                        
                        shooter.fName = [parseShooters objectAtIndex:x][@"fName"];
                        shooter.lName = [parseShooters objectAtIndex:x][@"lName"];
                        shooter.phone = [parseShooters objectAtIndex:x][@"phone"];
                        shooter.email = [parseShooters objectAtIndex:x][@"email"];
                        shooter.emergencyContact = [parseShooters objectAtIndex:x][@"emergencyContact"];
                        shooter.emergencyContactNum = [parseShooters objectAtIndex:x][@"emergencyContactNum"];
                        shooter.choke = [parseShooters objectAtIndex:x][@"choke"];
                        shooter.userId = [PFUser currentUser].objectId;
                       // NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", shooter.fName,shooter.lName]];
                        //int x =[Shooter MR_countOfEntities];
                        shooter.objectId = [parseShooters objectAtIndex:x][@"appObjectId"];
                    
                        
                        [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if (success) {
                                
                          //  NSLog(@"New Shooter Added to Local Storage");
                                
                            }
                        }];
                        
                        [self.shootersArray removeAllObjects];
                        self.shootersArray = [[Shooter MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
                        NSLog(@"shooters %@", shootersArray);
                        [_delegate retrieveData];
                        
                    }
                }
                else {
                   
                    [self.shootersArray removeAllObjects];
                    self.shootersArray = [[Shooter MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
                    [_delegate retrieveData];
                }
            }
            }];
    }else{ //else no connection
        
        [self.shootersArray removeAllObjects];
        self.shootersArray = [[Shooter MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
        [_delegate retrieveData];
        
        
    }

}
        
-(void) triggerRetrieveDataDelegate{
    

     shootersArray = [[NSMutableArray alloc] init];
    if([shootersArray count] <= 0 ){
        
        [self getShooters];
        
    }else{
        
        [_delegate retrieveData];
    }
    
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}



-(void) getCoaches {
    
     self.delegateTag = 3;
    BOOL connection = [self isNetworkAvailable];
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    
    if(connection){
        
       // NSLog(@"WE R HEREE");
        
        self.numeOfSavedCoaches = 0;
        self.numOfParseCoaches = 0;
        NSMutableArray *parseCoaches = [[NSMutableArray alloc] init];
        coachesArray = [[NSMutableArray alloc] init];
        
        self.coachesArray = [[Coach MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Coach"];
        [query whereKey:@"userId" equalTo: [PFUser currentUser].objectId];
        [query orderByDescending: @"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Successfully retrieved %lu coaches.", (unsigned long)objects.count);
                [parseCoaches addObjectsFromArray:objects];
                // Do something with the found objects
                
                self.numeOfSavedCoaches = [coachesArray count];
                self.numOfParseCoaches = [parseCoaches count];
                
                // NSLog(@"%@", parseShooters);
                
                
                
                
                if(self.numeOfSavedCoaches < self.numOfParseCoaches){
                    
                    int numberToUpdate = (self.numOfParseCoaches - self.numeOfSavedCoaches);
                    
                    for(int x = 0; x < numberToUpdate; x++){
                        
                        
                        Coach *coach = [Coach MR_createInContext:self.managedObjectContext];
                        coach.fName = [parseCoaches objectAtIndex:x][@"fName"];
                        coach.lName = [parseCoaches objectAtIndex:x][@"lName"];
                        coach.phone = [parseCoaches objectAtIndex:x][@"phone"];
                        coach.leftRightHanded = [parseCoaches objectAtIndex:x][@"leftRightHanded"];
                        coach.email = [parseCoaches objectAtIndex:x][@"email"];
                        //int x =[Coach MR_countOfEntities];
                       // NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", coach.fName,coach.lName]];
                        coach.objectId = [parseCoaches objectAtIndex:x][@"appObjectId"];
                        coach.userId = [PFUser currentUser].objectId;
                        
                        [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if (success) {
                                
                               // NSLog(@"New Shooter Added to Local Storage");
                                
                            }
                        }];
                        
                        [self.coachesArray removeAllObjects];
                        self.coachesArray = [[Coach MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
                             self.delegateTag = 3;
                        [_delegate retrieveData];
                        NSLog(@"coaches %@", coachesArray);
                        //[_delegate retrieveData];
                        
                    }
                }
                else {
                    
                    [self.coachesArray removeAllObjects];
                    self.coachesArray = [[Coach MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
                         self.delegateTag = 3;
                    [_delegate retrieveData];
                }
            }
        }];
    }else{ //else no connection
        
        [self.coachesArray removeAllObjects];
        self.coachesArray = [[Coach MR_findByAttribute: @"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"ascending:YES] mutableCopy];
             self.delegateTag = 3;
        [_delegate retrieveData];
        
        
    }
    
}
//Checks local storage and updates accourdingly from parse
-(void) getShoots {
    
    BOOL connection = [self isNetworkAvailable];
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    self.delegateTag = 1;
    
    if(connection){
        
       // NSLog(@"WE R HEREE");
        
        self.numOfSavedShoots = 0;
        self.numOfParseShoots = 0;
        NSMutableArray *parseShoots = [[NSMutableArray alloc] init];
        self.shootArray = [[NSMutableArray alloc] init];
        
         self.shootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Shoot"];
        [query whereKey:@"userID" equalTo: [PFUser currentUser].objectId];
        [query orderByDescending: @"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
               // NSLog(@"Successfully retrieved %lu coaches.", (unsigned long)objects.count);
                [parseShoots addObjectsFromArray:objects];
                // Do something with the found objects
                
                self.numOfSavedShoots = [self.shootArray count];
                self.numOfParseShoots = [parseShoots count];
                
                //NSLog(@"Parse Shoots:%@", parseShoots);
                
                
                
                
                if(self.numOfSavedShoots < self.numOfParseShoots){
                    
                    int numberToUpdate = (self.numOfParseShoots - self.numOfSavedShoots);
                   // NSLog(@"Number to update: %d", numberToUpdate);
                    
                    for(int x = 0; x < numberToUpdate; x++){
                        
                        
                        StoredShoot *storedShoot = [StoredShoot MR_createInContext:self.managedObjectContext];
                        storedShoot.coachId = [parseShoots objectAtIndex:x][@"coachID"];
                        storedShoot.typeShoot = [parseShoots objectAtIndex:x][@"typeShoot"];
                        storedShoot.totalShots = [parseShoots objectAtIndex:x][@"totalShots"];
                        storedShoot.shotsPerRotation = [parseShoots objectAtIndex:x][@"shotsPerRotation"];
                        NSDate *date = [[parseShoots objectAtIndex:x]createdAt];
                        storedShoot.trapFieldId = [parseShoots objectAtIndex:x][@"trapFieldId"];
                        storedShoot.trapHouseNumber = [parseShoots objectAtIndex:x][@"trapHouseNumber"];
                        storedShoot.handicap = [parseShoots objectAtIndex:x][@"handicap"];
                        storedShoot.dateCreated = date;
                        storedShoot.objectId = [parseShoots objectAtIndex:x][@"appObjectId"];
                        storedShoot.userId = [PFUser currentUser].objectId;
                        
                        [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if (success) {
                                
                              //  NSLog(@"New Shooter Added to Local Storage");
                                
                            }
                        }];
                        
                        [self.shootArray removeAllObjects];
                        self.shootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
                       // [_delegate retrieveData];
                       // NSLog(@"Shoots %@", coachesArray);
                        //[_delegate retrieveData];
                        
                    }
                }
                else {
                    
                    [self.shootArray removeAllObjects];
                     self.shootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
                    //[_delegate retrieveData];
                }
            }
        }];
    }else{ //else no connection
        
        [self.shootArray removeAllObjects];
         self.shootArray = [[StoredShoot MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
        //[_delegate retrieveData];
        
        
    }

    
}

//Checks local storage and updates accourdingly from parse
-(void) getShootShooters {
    
    BOOL connection = [self isNetworkAvailable];
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    self.delegateTag = 2;
    
    if(connection){
        
       // NSLog(@"getShootShooters");
        
        self.numOfSavedShootShooters = 0;
        self.numOfParseShootShooters = 0;
        NSMutableArray *shootShooter = [[NSMutableArray alloc] init];
        self.shootShooterArray = [[NSMutableArray alloc] init];
        
        [self.shootShooterArray removeAllObjects];
        self.shootShooterArray = [[ShootShooters MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
        //[_delegate retrieveData];
        
        PFQuery *query = [PFQuery queryWithClassName:@"ShootShooters"];
        [query whereKey:@"userID" equalTo: [PFUser currentUser].objectId];
        [query orderByDescending: @"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
              //  NSLog(@"Successfully retrieved %lu ShootShooters.", (unsigned long)objects.count);
                [shootShooter addObjectsFromArray:objects];
                // Do something with the found objects
                
                self.numOfSavedShootShooters = [self.shootShooterArray count];
                self.numOfParseShootShooters = [shootShooter count];
                
               // NSLog(@"Parse ShootShooters:%@", shootShooter);
                
                
                
                
                if(self.numOfSavedShootShooters < self.numOfParseShootShooters){
                    
                    int numberToUpdate = (self.numOfParseShootShooters - self.numOfSavedShootShooters);
                   // NSLog(@"Number to update: %d", numberToUpdate);
                    
                    for(int x = 0; x < numberToUpdate; x++){
                        
                     //Need to add userID to parse 
                        ShootShooters *storedShootShooter = [ShootShooters MR_createInContext:self.managedObjectContext];
                        storedShootShooter.shooterId = [shootShooter objectAtIndex:x][@"shooterID"];
                        storedShootShooter.storedShootId = [shootShooter objectAtIndex:x][@"shootID"];
                        storedShootShooter.startingPost = [shootShooter objectAtIndex:x][@"startingPost"];
                        NSString *tempData = [NSString stringWithFormat:@"%@", [[shootShooter objectAtIndex:x][@"shootData"] objectAtIndex:0]];
                        for(int i = 1; i < [[shootShooter objectAtIndex:x][@"shootData"] count]; i++){
                            
                            tempData = [NSString stringWithFormat:@"%@, %@", tempData, [[shootShooter objectAtIndex:x][@"shootData"] objectAtIndex:i]];
                            
                        }
                        storedShootShooter.totalHits = [shootShooter objectAtIndex:x][@"totalHits"];
                        storedShootShooter.scoreData = tempData;
                        storedShootShooter.totalShots = [shootShooter objectAtIndex:x][@"totalShots"];
                        storedShootShooter.userId = [PFUser currentUser].objectId;
                        
                        [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if (success) {
                                
                              //  NSLog(@"New ShootShooter Added to Local Storage");
                                
                            }
                        }];
                        
                        [self.shootShooterArray removeAllObjects];
                        self.shootShooterArray = [[ShootShooters MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
                       // [_delegate retrieveData];
                        NSLog(@"Shoots %@", coachesArray);
                        //[_delegate retrieveData];
                        
                    }
                }
                else {
                    
                    [self.shootShooterArray removeAllObjects];
                    self.shootShooterArray = [[ShootShooters MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
                   // [_delegate retrieveData];
                }
            }
        }];
    }else{ //else no connection
        
        [self.shootShooterArray removeAllObjects];
        self.shootShooterArray = [[ShootShooters MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId] mutableCopy];
       // [_delegate retrieveData];
        
        
    }
    
    
}


-(void) getTrapFields {
    
    BOOL connection = [self isNetworkAvailable];
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    self.delegateTag = 5;
    
    if(connection){
        
         NSLog(@"WE R HEREE");
        
        self.numOfSavedShoots = 0;
        self.numOfParseShoots = 0;
        NSMutableArray *parseFields= [[NSMutableArray alloc] init];
        self.trapFieldsArray = [[NSMutableArray alloc] init];
        
        self.trapFieldsArray = [[TrapField MR_findAllSortedBy:@"name" ascending:NO] mutableCopy];
        
        PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
        [query orderByDescending: @"Name"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                // NSLog(@"Successfully retrieved %lu coaches.", (unsigned long)objects.count);
                [parseFields addObjectsFromArray:objects];
                // Do something with the found objects
                
                self.numOfSavedShoots = [self.trapFieldsArray count];
                self.numOfParseShoots = [parseFields count];
                
                //NSLog(@"Parse Shoots:%@", parseShoots);
                
                
                
                
                if(self.numOfSavedShoots < self.numOfParseShoots){
                    
                    int numberToUpdate = (self.numOfParseShoots - self.numOfSavedShoots);
                     NSLog(@"Number to update: %d", numberToUpdate);
                    
                    for(int x = 0; x < numberToUpdate; x++){
                        
                        
                        TrapField *trapField = [TrapField MR_createInContext:self.managedObjectContext];
                        trapField.objectId = [[parseFields objectAtIndex:x] objectId];
                        NSLog(@"Object ID: %@", [parseFields objectAtIndex:x][@"objectId"]);
                        trapField.name = [parseFields objectAtIndex:x][@"Name"];
                        trapField.phone = [parseFields objectAtIndex:x][@"Phone"];
                        trapField.street = [parseFields objectAtIndex:x][@"Street"];
                        trapField.state = [parseFields objectAtIndex:x][@"State"];
                        trapField.zipCode = [parseFields objectAtIndex:x][@"ZipCode"];
                        trapField.city = [parseFields objectAtIndex:x][@"City"];
                        
                    
                        
                        
                        [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if (success) {
                                
                                //  NSLog(@"New Shooter Added to Local Storage");
                                
                            }
                        }];
                        
                        [self.trapFieldsArray removeAllObjects];
                        self.trapFieldsArray = [[TrapField MR_findAllSortedBy:@"name" ascending:YES] mutableCopy];
                        // [_delegate retrieveData];
                         NSLog(@"TrapFields %@", self.trapFieldsArray);
                        //[_delegate retrieveData];
                        
                    }
                }
                else {
                    
                    [self.trapFieldsArray removeAllObjects];
                    self.trapFieldsArray = [[TrapField MR_findAllSortedBy:@"name" ascending:YES] mutableCopy];
                    //[_delegate retrieveData];
                }
            }
        }];
    }else{ //else no connection
        
        [self.trapFieldsArray removeAllObjects];
       self.trapFieldsArray = [[TrapField MR_findAllSortedBy:@"name" ascending:YES] mutableCopy];
        //[_delegate retrieveData];
        
        
    }
    
    
}



@end
