//
//  Card.h
//  ClayCounter
//
//  Created by Test on 4/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//


#import "ScoringPage.h"
#import <UIKit/UIDevice.h>
#import "ShooterShootObject.h"
#import "Shooter.h"
#import "ShootShooters.h"
#import "StoredShoot.h"
#import <MessageUI/MessageUI.h>
#import "TrapField.h"
#import <MessageUI/MFMailComposeViewController.h>




@class ScoringPage;
@class ShooterShootObject;
@class Shooter;
@class StoredShoot;

@interface Card : UIViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) IBOutlet UILabel *titleInfoLabel;

@property ScoringPage *scoringPage;
-(void)showScoreCard : (int)index : (NSMutableArray *)shootShooters : (NSMutableArray *)shooterShootObjects : (StoredShoot *)storedShootTemp;
- (IBAction)cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sendEmail;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
- (IBAction)sendEmail:(id)sender;
@property (strong, nonatomic) StoredShoot *storedShoot;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) NSMutableArray *shooterShootObjectsArray;

@property (strong, nonatomic) NSMutableArray *reorederShootData;

@property int index1;


@end
