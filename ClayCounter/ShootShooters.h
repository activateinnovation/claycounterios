//
//  ShootShooters.h
//  ClayCounter
//
//  Created by Test on 2/27/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ShootShooters : NSManagedObject

@property (nonatomic, retain) NSString * shooterId;
@property (nonatomic, retain) NSString * storedShootId;
@property (nonatomic, retain) NSString * scoreData;
@property (nonatomic, retain) NSNumber *totalShots;
@property (nonatomic, retain) NSNumber *totalHits;
@property (nonatomic, retain) NSNumber *startingPost;
@property (nonatomic, retain) NSString *userId;
@end
