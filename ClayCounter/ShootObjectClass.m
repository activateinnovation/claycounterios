//
//  ShootObjectClass.m
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ShootObjectClass.h"

@implementation ShootObjectClass
@synthesize shootersArray;
@synthesize coach;
@synthesize shotsTotal;
@synthesize shotsAtEachPost;

-(ShootObjectClass *) initWithItems: (NSMutableArray *) shooters : (Coach *) coachTemp : (int) totalShots : (int) shotsPerPost{
    
    
    self.shootersArray = [[[NSMutableArray alloc] initWithArray:shooters] mutableCopy];
    self.coach  = coachTemp;
    self.shotsTotal = totalShots;
    self.shotsAtEachPost = shotsPerPost;
    
    return self;
}


@end
