//
//  ShootObjectClass.h
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Coach.h"

@interface ShootObjectClass : NSObject

@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) Coach *coach;
@property int shotsTotal;
@property int shotsAtEachPost;
@property (strong, nonatomic) NSString *pracOrComp;
-(ShootObjectClass *) initWithItems: (NSMutableArray *) shooters : (Coach *) coachTemp : (int) totalShots : (int) shotsPerPost;

           
@end
