//
//  ScoringShootPage.m
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ScoringShootPage.h"

@interface ScoringShootPage ()

@end

@implementation ScoringShootPage
@synthesize shootObject;
@synthesize tableView;
@synthesize leftPlusButton;
@synthesize leftButton;
@synthesize straightButton;
@synthesize rightButton;
@synthesize rightPlusButton;
@synthesize hitButton;
@synthesize counter;
@synthesize setUpShoot;
@synthesize currentShoot;
@synthesize managedObjectContext;
@synthesize coach;
@synthesize testButton;
@synthesize shotsCounter;
@synthesize shooterIndex;
@synthesize shooterShootArray;
@synthesize roundNumber;
@synthesize endAlert;
@synthesize tempShootId;
@synthesize previousShooterIndex;
@synthesize modifyScoreTag;
@synthesize undoButton;
@synthesize startingPostNumbers;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated{

    
    self.coachId = [[NSString alloc] init];
    self.shooterShootArray = [[NSMutableArray alloc] init];
    self.shootersArray = [[NSMutableArray alloc] init]; //CAN GET RID OF THIS ONCE SHOOTSHOOTOBJECT IS IMPLEMENTED BECAUSE THE SHOOTER OBJECT WILL BE IN THERE
    self.currentShoot = [CurrentShoot MR_findFirst];
    
    NSLog(@"Current Shoot: %@ , %@", self.currentShoot.trapFieldId, self.currentShoot.trapHouseNumber);
    self.shotsCounter = 1;
    self.shooterIndex = 0;
    self.previousShooterIndex = 0;
    self.roundNumber = 0;
    self.modifyScoreTag = -1;
    self.tempShootId = [[NSString alloc] init];
   //  NSLog(@"TypeOfShoot: %@, ShotsPerRotation: %@", self.currentShoot.pracOrComp, self.currentShoot.shotsAtPost);
    //NSLog(@"Total Shots: %@, Shots per Post: %@", self.currentShoot.totalShots,  self.currentShoot.shotsAtPost);
    //self.coach = [[Coach alloc] init];
     self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    //Checks the coach
    self.coach = [Coach MR_findFirstByAttribute:@"objectId" withValue: self.currentShoot.coach inContext:self.managedObjectContext];
    self.coachId = self.coach.objectId;
   // NSLog(@"CoachObjectId: %@", self.coach.objectId);
    self.startingPostNumbers = [[NSMutableArray alloc] init];
    NSArray *temp = [self.currentShoot.startingPost componentsSeparatedByString:@","];
    self.startingPostNumbers = [NSMutableArray arrayWithArray:temp];
    

    if([self.coach.leftRightHanded isEqualToString:@"Left"]){
        
       // NSLog(@"Left");
        if(self.view.frame.size.width > 320){
            
            self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(60, 61, (self.view.frame.size.width - 60), self.view.frame.size.height)];
           /* [self.hitButton setFrame:CGRectMake(0, 246, 60, 84)];
            [self.leftPlusButton setFrame:CGRectMake(0, 331, 60, 84)];
            [self.leftButton setFrame:CGRectMake(0, 416, 60, 84)];
            [self.straightButton setFrame:CGRectMake(0, 501, 60, 84)];
            [self.rightButton setFrame:CGRectMake(0, 586, 60, 84)];
            [self.rightPlusButton setFrame:CGRectMake(0, 672, 60, 84)];*/
            
            [self.undoButton setFrame:CGRectMake(0, 246, 60, 60)];
            [self.leftPlusButton setFrame:CGRectMake(0, 309, 60, 60)];
            [self.leftButton setFrame:CGRectMake(0, 372, 60, 60)];
            [self.hitButton setFrame:CGRectMake(0, 440, 60, 115)];
            [self.straightButton setFrame:CGRectMake(0, 563, 60, 60)];
            [self.rightButton setFrame:CGRectMake(0, 626, 60, 60)];
            [self.rightPlusButton setFrame:CGRectMake(0, 689, 60, 60)];
            
        }
        else{
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(60, 61, 260, 504)];
        [self.undoButton setFrame:CGRectMake(0, 61, 60, 60)];
        [self.leftPlusButton setFrame:CGRectMake(0, 124, 60, 60)];
        [self.leftButton setFrame:CGRectMake(0, 187, 60, 60)];
        [self.hitButton setFrame:CGRectMake(0, 257, 60, 115)];
        [self.straightButton setFrame:CGRectMake(0, 382, 60, 60)];
        [self.rightButton setFrame:CGRectMake(0, 445, 60, 60)];
        [self.rightPlusButton setFrame:CGRectMake(0, 508, 60, 60)];
        }

    }
    else if([self.coach.leftRightHanded isEqualToString:@"Right"]){
        
     //RIGHT
       // NSLog(@"Right");
        if(self.view.frame.size.width > 320){
            
            self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 61, (self.view.frame.size.width - 60), self.view.frame.size.height)];
     
            [self.undoButton setFrame:CGRectMake(self.tableView.frame.size.width, 246, 60, 60)];
            [self.leftPlusButton setFrame:CGRectMake(self.tableView.frame.size.width, 309, 60, 60)];
            [self.leftButton setFrame:CGRectMake(self.tableView.frame.size.width, 372, 60, 60)];
            [self.hitButton setFrame:CGRectMake(self.tableView.frame.size.width, 440, 60, 115)];
            [self.straightButton setFrame:CGRectMake(self.tableView.frame.size.width, 563, 60, 60)];
            [self.rightButton setFrame:CGRectMake(self.tableView.frame.size.width, 626, 60, 60)];
            [self.rightPlusButton setFrame:CGRectMake(self.tableView.frame.size.width, 689, 60, 60)];
            
        }
        else{
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 61, 260, 504)];
            [self.undoButton setFrame:CGRectMake(260, 61, 60, 60)];
            [self.leftPlusButton setFrame:CGRectMake(260, 124, 60, 60)];
            [self.leftButton setFrame:CGRectMake(260, 187, 60, 60)];
            [self.hitButton setFrame:CGRectMake(260, 257, 60, 115)];
            [self.straightButton setFrame:CGRectMake(260, 382, 60, 60)];
            [self.rightButton setFrame:CGRectMake(260, 445, 60, 60)];
            [self.rightPlusButton setFrame:CGRectMake(260, 508, 60, 60)];
        }

    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
     tempArray = [[self.currentShoot.shooters componentsSeparatedByString:@","] mutableCopy];
    //NSLog(@"tempArray%@", tempArray);

    
    //this is where the starting post number gets set in the view for ecah shooter
    for(int x = 0; x < tempArray.count; x++){
        
        Shooter *shooter = [Shooter MR_findFirstByAttribute:@"objectId" withValue:[tempArray objectAtIndex:x]];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        ShooterShootObject *shooterShootObject = [[ShooterShootObject alloc] initWithItems:shooter : [[self.startingPostNumbers objectAtIndex:x] intValue] : 0 : 0 : tempArray : 0 : 0];
        
        [self.shooterShootArray addObject:shooterShootObject];
        
    }
    
    
    
    [self.view addSubview:self.undoButton];
    [self.view addSubview:self.leftPlusButton];
    [self.view addSubview:self.leftButton];
    [self.view addSubview:self.straightButton];
    [self.view addSubview:self.rightPlusButton];
    [self.view addSubview:self.rightButton];
    [self.view addSubview:self.hitButton];
    
    [self.view addSubview:self.tableView];
   

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     //[self.view addSubview:self.tableView];

    //[self.tableView reloadData];
   
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.setUpShoot =
    [storyboard instantiateViewControllerWithIdentifier:@"setupShoot"];
    
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   // NSLog(@"NumberOfRows");
    return [self.shooterShootArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    CustomShootCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    ShooterShootObject *shooterShootObject = [self.shooterShootArray objectAtIndex:indexPath.row];
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    if (cell == nil) {
        
        
        //NSLog(@"TotatlShots: %@", self.currentShoot.totalShots);
        cell = [[CustomShootCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier : [NSString stringWithFormat:@"%@", self.currentShoot.totalShots] : [NSString stringWithFormat:@"%d", shooterShootObject.shotCounter] : [NSString stringWithFormat:@"%d", shooterShootObject.postNumber] : [NSString stringWithFormat:@"%@ %@", shooterShootObject.shooter.fName, shooterShootObject.shooter.lName]: self.currentShoot.shotsAtPost :  shooterShootObject.hitNumber : shooterShootObject.scoreArray: shooterShootObject.roundNumber: shooterShootObject.shotsFiredInRound : self.shooterShootArray];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
       
    }
    
  
    cell.delegate = self;
    cell.tag = indexPath.row;
   
   // NSLog(@"ShooterIndex: %d", shooterIndex);
    
    if(indexPath.row == self.shooterIndex){
        
        [cell setBackgroundColor: [self colorWithHexString:@"D0D0D0"]];
        //[cell setAlpha:0.1];
    }
    
    
    if(self.shotsCounter == 1)
    {
        self.undoButton.enabled = NO;
    
    }else if(shooterIndex == 0 && (cell.shotsFiredInRound == cell.shotsAtEachPost || cell.shotsFiredInRound == 0)){
        //NSLog(@"HEREYeah");
            self.undoButton.enabled = NO;
            
    }else{
        
          self.undoButton.enabled = YES;
    }
    ;
    
    
    
    /*if((self.shotCounter * 3) == (cell.roundNumber * cell.shotsAtEachPost)){
        
        for(int i = 0; i < cell.textFieldArray.count; i++){
            
            [[cell.textFieldArray objectAtIndex:i]setText: @""];
            
        }

    }*/
 
    return cell;
    
}

-(void) viewDidDisappear:(BOOL)animated{
    
    [self.leftPlusButton removeFromSuperview];
    [self.leftButton removeFromSuperview];
    [self.straightButton removeFromSuperview];
    [self.rightButton removeFromSuperview];
    [self.rightPlusButton removeFromSuperview];
    [self.leftPlusButton removeFromSuperview];
    [self.hitButton removeFromSuperview];
    self.coach = nil;
    
    
}


- (IBAction)cancelShoot:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel Shoot" message:@"Are you sure you want to cancel your current shoot?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"Resume", nil];
    alert.tag = 3;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    
    /*NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSArray *tempShoot = [CurrentShoot MR_findAll];
    
    for(int x = 0; x < tempShoot.count; x++){
        
        CurrentShoot *temp = [tempShoot objectAtIndex:x];
        [temp MR_deleteInContext:localContext];
    }

    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if(success) {
            NSLog(@"Success deleting object");
            
            [self presentViewController:self.setUpShoot animated:YES completion:nil];
        }
    }];*/
  
    
}


-(IBAction)scoringButtonCLicked:(id)sender{
    
    
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:NO];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:self.shooterIndex inSection:0];
    CustomShootCell *cell = (CustomShootCell *)[self.tableView cellForRowAtIndexPath: index];
  /*  [cell setBackgroundColor:[UIColor orangeColor]];
    [cell setAlpha:0.7];
    [self.tableView reloadData];*/

   // [self goToCurrentCell];
    ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:self.shooterIndex];
    
       // UITextField *textFieldTemp = [cell.textFieldArray objectAtIndex:shooterShootTemp.shotsFiredInRound];
    
   // NSLog(@"ShooterShootArray: %@", self.shooterShootArray);
    
    if(modifyScoreTag > -1){
        
        
    
    }else{
        
        if(shooterShootTemp.shotsFiredInRound == cell.shotsAtEachPost ){
        
        if(shooterShootTemp.postNumber == 5){
            
            shooterShootTemp.postNumber = 1;
            
        }
        else{
            shooterShootTemp.postNumber++;
        
        }
        shooterShootTemp.roundNumber++;
        shooterShootTemp.shotsFiredInRound = 0;
        shooterShootTemp.hitsInRound = 0;
        for (int x = 0; x < cell.tempScores.count; x++){
                
                [cell.tempScores replaceObjectAtIndex:x withObject:@""];
                
        }
    }
        
        
    }
    
   // NSLog(@"ModifyScoreTagOnClick: %d", self.modifyScoreTag);
    //Need to upate the shots fired in post variable
   // NSLog(@"Shooter: %@", shooterShootTemp.shooter.fName);
    //NSLog(@"shooterIndex: %d", shooterIndex);
    
    if(sender == self.leftPlusButton){
      //  NSLog(@"leftPlusButton");
        //[textFieldTemp setTextColor:[UIColor redColor]];
       //textFieldTemp.text = @"L+";
        if(self.modifyScoreTag > -1){
            //NEED TO CHECK FOR ROUND NUMBER
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"L+"];
            
        }else{
        
            [shooterShootTemp.scoreArray addObject:@"L+"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
       
        }
        [self.tableView reloadData];
        
    }else if (sender == self.leftButton){
      //  NSLog(@"leftButton");
        //[textFieldTemp setTextColor:[UIColor redColor]];
        //textFieldTemp.text = @"L";
        if(self.modifyScoreTag > -1){
            
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"L"];
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"L"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
       [self.tableView reloadData];
    
    }else if(sender == self.straightButton){
       // NSLog(@"straightButton");
       // [textFieldTemp setTextColor:[UIColor redColor]];
        //textFieldTemp.text = @"S";
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"S"];
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"S"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
       [self.tableView reloadData];
        
    }else if(sender == self.rightButton){
       // NSLog(@"rightButton");
        //[textFieldTemp setTextColor:[UIColor redColor]];
       //textFieldTemp.text = @"R";
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"R"];
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"R"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
        [self.tableView reloadData];
        
    }else if(sender == self.rightPlusButton){
       // NSLog(@"rightPlusButton");
       // [textFieldTemp setTextColor:[UIColor redColor]];
       // textFieldTemp.text = @"R+";
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"R+"];
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"R+"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
       [self.tableView reloadData];
        
    }else if(sender == self.hitButton){
       // NSLog(@"hitButton");
       // [textFieldTemp setTextColor:[UIColor greenColor]];
        //textFieldTemp.text = @"H";
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex: self.modifyScoreTag withObject:@"H"];
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"H"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
        shooterShootTemp.hitNumber++;
        shooterShootTemp.hitsInRound++;
       
        [self.tableView reloadData];
        
    }
    else{
        
        
    }
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:NO];
 
   // NSLog(@"Shooter index: %d", self.shooterIndex);
    
    
    int temp = [self.shooterShootArray count];
    if(cell.roundNumber == 0){
        
        if(self.shotsCounter == (cell.shotsAtEachPost * temp) + 1){
            
            NSString *tempHits = [[NSString alloc] init];
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter.fName, shootTemp.hitsInRound];
            }
            
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            alert.tag = 1;
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            
            
        }
        
    }
    else{
        
        if(shooterIndex == ([self.shooterShootArray count] - 1) && (cell.tempScores.count == cell.shotsAtEachPost - 1)){
            
            
            //ENDS THE ROUND
            if((self.shotsCounter - 1) == [self.shooterShootArray count]  * [self.currentShoot.totalShots intValue]) {
                
                NSString *tempHits = [[NSString alloc] init];
                int squadTotal = 0;
                
                for (int x = 0; x < shooterShootArray.count; x++) {
                    ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                  //  NSLog(@"Hit Number: %d", shootTemp.hitNumber);
                    tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits/ %d Total", tempHits ,shootTemp.shooter.fName, shootTemp.hitNumber, [currentShoot.totalShots intValue]];
                    
                    squadTotal += shootTemp.hitNumber;
                }
                
                tempHits = [NSString stringWithFormat:@"%@\nSquad Total: %d", tempHits, squadTotal];
                
                self.endAlert = [[UIAlertView alloc] initWithTitle:@"End of Shoot Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [self.endAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                
                
            }else{
                
                NSString *tempHits = [[NSString alloc] init];
                
                for (int x = 0; x < shooterShootArray.count; x++) {
                    ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                    tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter.fName, shootTemp.hitsInRound];
                }
                
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag =2;
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            }
        }
        
        
    }

    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:YES];
    
    
    
    if(modifyScoreTag > -1){
        
        self.shooterIndex = previousShooterIndex;
    
    }
    
    
    [self enableOrDisableShootButtons: NO];
    [self performSelector:@selector(goToCurrentCell) withObject:nil afterDelay:0.01];
   
    
}




-(void) goToCurrentCell{
    
  //  NSLog(@"HERE");
    if(modifyScoreTag > -1){
        
        
    }else{
    
        if(shooterIndex == ([self.shooterShootArray count] - 1)){
       
            shooterIndex = 0;
        
        }
        else{
    
            
            shooterIndex++;
    
        }
    }


     NSIndexPath *index1 = [NSIndexPath indexPathForRow:self.shooterIndex inSection:0];
       [tableView scrollToRowAtIndexPath:index1 atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self enableOrDisableShootButtons:YES];
    self.previousShooterIndex = 0;
    self.modifyScoreTag = -1;
    [self.tableView setUserInteractionEnabled:YES];
     [self.tableView reloadData];
    
}

//This sections saves the shoot data to parse and to core data
-(void) endAndSaveShoot{
    
    //Creates a string of date time to hash and create a unique id for each shoot object
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmm"];
    dateString = [formatter stringFromDate:[NSDate date]];
   // NSLog(@"DateTimeString: %@", dateString);

    
    //Hash the date string and set that as the appObjectId in parse
    self.tempShootId = [self sha1:dateString];
    
   // self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    //NSLog(@"CoacchID2: %@", self.coach.objectId);
    StoredShoot *storedShoot = [StoredShoot MR_createInContext:self.managedObjectContext];
    storedShoot.coachId = self.coach.objectId;
    storedShoot.totalShots = self.currentShoot.totalShots;
    storedShoot.typeShoot = self.currentShoot.pracOrComp;
    storedShoot.shotsPerRotation = self.currentShoot.shotsAtPost;
    storedShoot.trapFieldId = self.currentShoot.trapFieldId;
    storedShoot.trapHouseNumber = self.currentShoot.trapHouseNumber;
    storedShoot.handicap = self.currentShoot.handicap;
    storedShoot.dateCreated = [NSDate date];
    storedShoot.userId = [PFUser currentUser].objectId;
    //int x = [Shooter MR_countOfEntities];
    storedShoot.objectId = self.tempShootId;//[NSNumber numberWithInt:(x + 1)];
    
    
    [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
    
            // NSLog(@"TypeOfShoot: %@, CoachId: %@, UserId: %@, TempShootId: %@", self.currentShoot.pracOrComp, self.coachId, [PFUser currentUser].objectId, tempShootId);
            PFObject *shoot = [PFObject objectWithClassName:@"Shoot"];
            shoot[@"userID"] = [PFUser currentUser].objectId;
            shoot[@"coachID"] = self.coachId;
            shoot[@"appObjectId"] = tempShootId;
            shoot[@"trapFieldId"] = self.currentShoot.trapFieldId;
            shoot[@"handicap"] = self.currentShoot.handicap;
            shoot[@"trapHouseNumber"] = self.currentShoot.trapHouseNumber;
            shoot[@"typeShoot"] = self.currentShoot.pracOrComp;
            shoot[@"shotsPerRotation"] = self.currentShoot.shotsAtPost;
            shoot[@"totalShots"] = self.currentShoot.totalShots;
    
            [shoot saveEventually];
            
            NSLog(@"ShooterShootArray: %@ Count: %lu", self.shooterShootArray, (unsigned long)self.shooterShootArray.count);
            
            for(int x = 0; x < shooterShootArray.count; x++){
                
                ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:x];
                Shooter *shooterTemp = shooterShootTemp.shooter;
                //NSLog(@"HERE : %@", self.shooterShootArray);
                ShootShooters *shootShooter = [ShootShooters MR_createInContext:self.managedObjectContext];
                shootShooter.startingPost = [NSNumber numberWithInt:[[self.startingPostNumbers objectAtIndex:x] intValue]];
                shootShooter.storedShootId = tempShootId;
                shootShooter.totalShots = self.currentShoot.totalShots;
                shootShooter.userId = [PFUser currentUser].objectId;
                shootShooter.shooterId = [NSString stringWithFormat:@"%@", shooterTemp.objectId];
                int hitCounter = 0;
                for(int j = 0; j < shooterShootTemp.scoreArray.count; j++){
                    if([[shooterShootTemp.scoreArray objectAtIndex:j] isEqualToString: @"H"]){
                        
                        hitCounter++;
                    }
                    shootShooter.totalHits = [NSNumber numberWithInt:hitCounter];
                    
                    
                }
                NSString *tempData = [NSString stringWithFormat:@"%@", [shooterShootTemp.scoreArray objectAtIndex:0]];
                for(int i = 1; i < shooterShootTemp.scoreArray.count; i++){
                    
                    tempData = [NSString stringWithFormat:@"%@, %@", tempData, [shooterShootTemp.scoreArray objectAtIndex:i]];
                    
                }
                shootShooter.scoreData = tempData;
                //NSLog(@"Score String: %@", tempData);
                
                [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    if (!error) {
                        
                        PFObject *shooterShoot = [PFObject objectWithClassName:@"ShootShooters"];
                        shooterShoot[@"shootID"] = tempShootId;
                        shooterShoot[@"totalShots"] = self.currentShoot.totalShots;
                        shooterShoot[@"shooterID"] = [NSString stringWithFormat:@"%@", shooterTemp.objectId];
                        shooterShoot[@"startingPost"] = [NSNumber numberWithInt:[[self.startingPostNumbers objectAtIndex:x] intValue]];

                        shooterShoot[@"shootData"] = shooterShootTemp.scoreArray;
                        shooterShoot[@"totalHits"] = [NSNumber numberWithInt: hitCounter];
                        shooterShoot[@"userID"] = [PFUser currentUser].objectId;
                        [shooterShoot saveEventually];
                        
                   
                    }}];

            }}}];
     [self presentViewController:self.scoringPage animated:YES completion:nil];
    
   // NSLog(@"endAndSaveShoot");
    
            
    
    
   
}

-(void) enableOrDisableShootButtons: (BOOL) enableDisable{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    });
    //NSLog(@"GOTHERE");
    //Buttons are enabled with YES
    if(enableDisable){
        [self.leftPlusButton setEnabled: YES];
        [self.leftButton setEnabled: YES];
        [self.straightButton setEnabled: YES];
        [self.rightButton setEnabled: YES];
        [self.rightPlusButton setEnabled: YES];
        [self.hitButton setEnabled: YES];
    }
    else{ //Buttons are disabled with NO
        [self.leftPlusButton setEnabled: NO];
        [self.leftButton setEnabled: NO];
        [self.straightButton setEnabled: NO];
        [self.rightButton setEnabled: NO];
        [self.rightPlusButton setEnabled: NO];
        [self.hitButton setEnabled: NO];
        
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView == self.endAlert){
        
        if(buttonIndex == 0){
            
            [self endAndSaveShoot];
            
            
        }
        
        
    }
    else if(alertView.tag == 1){
        
        
        
    }
    else if(alertView.tag == 2){
        
        
        
    }
    else if(alertView.tag == 3)
    {
        
        if(buttonIndex == 0)
        {
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            NSArray *tempShoot = [CurrentShoot MR_findAll];
            
            for(int x = 0; x < tempShoot.count; x++){
                
                CurrentShoot *temp = [tempShoot objectAtIndex:x];
                [temp MR_deleteInContext:localContext];
            }
            
            [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if(success) {
                    //NSLog(@"Success deleting object");
                    
                    [self presentViewController:self.setUpShoot animated:YES completion:nil];
                }
            }];

            
        }
        else
        {
            
            
        }
        
        
    }

}


//This allows the user to go back and rerecord a shoot score
- (IBAction)undoLastAction:(id)sender {

    
   // NSLog(@"ShooterIndex: %d", self.shooterIndex);
    
  
    if(self.shooterIndex == 0){
        
        self.shooterIndex = ([self.shooterShootArray count] - 1);
       // NSLog(@"UNDO");
        
    }else{
        
        self.shooterIndex--;
        
    }
    
    
    
   // NSLog(@"ShooterIndexv2: %d", self.shooterIndex);
    NSIndexPath *indexCell = [NSIndexPath indexPathForRow: self.shooterIndex inSection:0];
    CustomShootCell *cellChange = (CustomShootCell *)[self.tableView cellForRowAtIndexPath: indexCell];
     ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:self.shooterIndex];
    
    
    
     if(cellChange.shotsFiredInRound == cellChange.shotsAtEachPost){
        
       // NSLog(@"ROUNDDOWN");
         self.undoButton.enabled = NO;
        //Set Undo button to disable so that you cant undo past a round
        
    }
    
    /*[self.tableView reloadData];*/
    cellChange.shotsFiredInRound--;
    cellChange.shotsFired--;
    
    if([[cellChange.scoreArray objectAtIndex:([cellChange.scoreArray count] - 1)] isEqualToString:@"H"]){
        
        shooterShootTemp.hitNumber--;
        shooterShootTemp.hitsInRound--;
        cellChange.hitsOverall--;
        
    }
  
    [cellChange.scoreArray removeLastObject];
    self.shotsCounter--;

   
    shooterShootTemp.shotsFiredInRound--;
    shooterShootTemp.shotCounter--;
  

    [tableView scrollToRowAtIndexPath:indexCell atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self.tableView reloadData];
    [self enableOrDisableShootButtons:YES];
    
   
    // UITextField *textFieldTemp = [cell.textFieldArray objectAtIndex:shooterShootTemp.shotsFiredInRound];
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

/*-(void)textFieldClicked: (CustomShootCell *)cell : (UITextField *)textField : (NSMutableArray *)shootShooter{
    
    CustomShootCell *shootCell = cell;
    
    //[cell setBackgroundColor: [UIColor orangeColor]];
    //[cell setAlpha:0.1];
    
    self.shooterIndex = cell.tag;
   
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:NO];

    
    NSLog(@"SHOOTERINDEX: %d", self.shooterIndex);
    
 
    ShooterShootObject *shooterShootObject = [shootShooter objectAtIndex:self.shooterIndex];
    
    
    shooterShootObject.shotCounter--;
    shooterShootObject.shotsFiredInRound--;
    
    shootCell.shotsFired--;
    shootCell.shotsFiredInRound--;
    self.shotsCounter--;
    self.shotCounter--;
    if([textField.text isEqualToString:@"H"]){
        
        shootCell.hitsOverall--;
        shooterShootObject.hitNumber--;
        shooterShootObject.hitsInRound--;
    }
    
    [textField setText:@""];
    [shooterShootObject.scoreArray removeObjectAtIndex:textField.tag];
    
    NSLog(@"TextFieldScoring: %d Cell: %d", textField.tag, shootCell.tag);
    
    
    [self enableOrDisableShootButtons:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    });
    
}*/

-(void)scoreFieldClicked : (int)cellTag :(UITextField *)textField :(NSMutableArray *)shootShooter{
    
    [self.tableView setUserInteractionEnabled:NO];
    self.previousShooterIndex = self.shooterIndex;
    
    
    
   // NSLog(@"Delegate called properly");
    NSIndexPath *cellIndex = [NSIndexPath indexPathForRow:cellTag inSection:0];
    
    CustomShootCell *shootCell = (CustomShootCell *)[tableView cellForRowAtIndexPath:cellIndex];
    
    
    self.shooterIndex = cellTag;
    ShooterShootObject *shooterShootObject = [shootShooter objectAtIndex:self.shooterIndex];
    

    
    [tableView scrollToRowAtIndexPath:cellIndex atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self.tableView reloadData];
    
   // NSLog(@"shootCell.RoundNumber: %d", shootCell.roundNumber);
    int x;
    if(shootCell.roundNumber == 0){
        
        if([textField.text isEqualToString:@"H"]){
            
            shooterShootObject.hitNumber--;
            shooterShootObject.hitsInRound--;
        }
        
          self.modifyScoreTag = textField.tag;
        [shooterShootObject.scoreArray replaceObjectAtIndex:textField.tag withObject:@""];
        [shootCell.scoreArray replaceObjectAtIndex:textField.tag withObject:@""];
    }
    else{
        
        if([textField.text isEqualToString:@"H"]){
            
            shooterShootObject.hitNumber--;
            shooterShootObject.hitsInRound--;
        }
        
        x = (shootCell.roundNumber *shootCell.shotsAtEachPost) + textField.tag;
        //NSLog(@"number for round: %d", x);
        
        self.modifyScoreTag = x;
        [shooterShootObject.scoreArray replaceObjectAtIndex:x withObject:@""];
        [shootCell.scoreArray replaceObjectAtIndex:x withObject:@""];
        
        
    }
    
    
    
     //NSLog(@"ScoreArrayAfter: %@", shootCell.scoreArray);
    
   // NSLog(@"SHOOTERINDEX: %d", self.shooterIndex);
    
    
    
   // [shooterShootObject.scoreArray removeObjectAtIndex:textField.tag];
   // [shootCell.scoreArray removeObjectAtIndex:textField.tag];
    //shooterShootObject.shotCounter--;
   // shooterShootObject.shotsFiredInRound--;
    
   // shootCell.shotsFired--;
    //shootCell.shotsFiredInRound--;
   // self.shotsCounter--;
    //self.shotCounter--;
   
    
  
   // NSLog(@"ModifyScoreTag: %d", self.modifyScoreTag);
    
   // [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
 

  
   // [self enableOrDisableShootButtons:YES];

}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}



@end
