//
//  UpdatedData.h
//  ClayCounter
//
//  Created by Test on 1/18/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

@protocol UpdateDataDelegate

-(void)retrieveData;

@end

#import "SettingsPage.h"
#import "ScoringPage.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "ShootersPage.h"
#import <CoreData/CoreData.h>
#import "Shooter.h"
#import "Coach.h"
#import "ShootShooters.h"
#import "TrapField.h"

@interface UpdatedData : NSObject

-(void) triggerRetrieveDataDelegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
-(BOOL) isNetworkAvailable;
@property (weak, nonatomic) id <UpdateDataDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) NSMutableArray *coachesArray;
@property (strong, nonatomic) NSMutableArray *shootArray;
@property (strong, nonatomic) NSMutableArray *shootShooterArray;
@property (strong, nonatomic) NSMutableArray *trapFieldsArray;
@property int numOfSavedShooters;
@property int numOfParseShooters;
@property int numeOfSavedCoaches;
@property int numOfParseCoaches;
@property int numOfSavedShoots;
@property int numOfParseShoots;
@property int numOfSavedShootShooters;
@property int numOfParseShootShooters;
@property int delegateTag;
-(void)getShooters;
-(void)getCoaches;
-(void) getShoots;
-(void) getShootShooters;
-(void) getTrapFields;
@end
