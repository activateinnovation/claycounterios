//
//  ShootersPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ShootersPage.h"
#include<unistd.h> 
#include<netdb.h>

@interface ShootersPage ()

@end

@implementation ShootersPage
@synthesize tableView;
@synthesize scoringPage;
@synthesize settingsPage;
@synthesize stayLoggedIn;
@synthesize logInViewController;
@synthesize signUpViewController;
@synthesize userDefaults;
@synthesize container;
@synthesize addShooterView;
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNum;
@synthesize emailAddress;
@synthesize emergencyContact;
@synthesize emergencyContactNum;
@synthesize chokeType;
@synthesize shootersArray;
@synthesize managedObjectContext;
@synthesize connection;
@synthesize parseShooters;
@synthesize numOfParseShooters;
@synthesize numOfSavedShooters;
@synthesize appDelegate;
@synthesize updatedData;
@synthesize spinner;
@synthesize cancelAddShooterOutlet;
@synthesize navBar;
@synthesize addShooterOutlet;
@synthesize shooterToDelete;
@synthesize indexPathToDelete;
@synthesize editShooter;



- (void) viewWillAppear:(BOOL)animated{
    
    //Pulls all current shooters from persistent memory and sorts them by firstname from the Core Data database
   
    self.parseShooters = [[NSMutableArray alloc] init];
    
    self.shootersArray = [[NSMutableArray alloc] init];
    self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];

    [spinner startAnimating];
   

    self.cancelAddShooterOutlet.enabled = NO;
    [self.updatedData getShooters];
    [self.updatedData getCoaches];
    [self.updatedData getShoots];
     [self.updatedData getShootShooters];
   
    
    
   // self.shootersArray = [[Shooter MR_findAllSortedBy:@"fName" ascending:YES] mutableCopy];
   
   /* self.connection = [self isNetworkAvailable];
    
    if(self.connection){
        
        
        PFQuery *query = [PFQuery  queryWithClassName:@"Shooter"];
        [query whereKey:@"userId" equalTo: [PFUser currentUser].objectId];
        [query orderByDescending: @"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Successfully retrieved %d scores.", objects.count);
                [self.parseShooters addObjectsFromArray:objects];
                // Do something with the found objects
                
                self.numOfSavedShooters = [shootersArray count];
                self.numOfParseShooters = [parseShooters count];
            
                [self updateData: self.numOfSavedShooters  : self.numOfParseShooters];

            } else {
                // Log details of the failure
                NSLog(@"Error: %@ %@", error, [error userInfo]);
            }
        }];
        
    }*/
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tabBar.delegate = self;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
    
    UIStoryboard* storyboard2 = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    self.settingsPage =
    [storyboard2 instantiateViewControllerWithIdentifier:@"Settings"];
   
   [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 0]];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"%@", [PFUser currentUser].username);
    
    //self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, 320, 520)];
    self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];
    self.addShooterView = [[UIView alloc] initWithFrame:self.container.bounds];
    self.addShooterView.clearsContextBeforeDrawing = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.addShooterView addGestureRecognizer:tap];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    //id delegate = [[UIApplication sharedApplication] delegate];
   ;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) retrieveData{
    
    [self.shootersArray removeAllObjects];
    self.shootersArray = [self.updatedData.shootersArray mutableCopy];
    NSLog(@"ShootersPageData: %@", self.shootersArray);
   
    [self.tableView reloadData];
    [spinner stopAnimating];
    
    
    
}


#pragma mark TABLEVIEW DELEGATE METHODS
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.shootersArray count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    MSCMoreOptionTableViewCell *cell = (MSCMoreOptionTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[MSCMoreOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
        cell.delegate = self;
    
    }
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    Shooter *shooterTemp = [self.shootersArray objectAtIndex:indexPath.row];
    
    UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 7, 180, 25)];
    [cellTitle setFont:[UIFont boldSystemFontOfSize:18]];
    [cellTitle setText: [NSString stringWithFormat:@"%@ %@",shooterTemp.fName, shooterTemp.lName]];
    UILabel *shooterPhone = [[UILabel alloc] initWithFrame:CGRectMake(170, 7, 180, 25)];
    [shooterPhone setTextColor:[UIColor darkGrayColor]];
    [shooterPhone setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp.phone isEqualToString:@""]){
        shooterPhone.text = @"Phone: NA";
    }else{
        shooterPhone.text = [NSString stringWithFormat:@"Phone: %@", shooterTemp.phone];
    }
    
    UILabel *shooterEmail = [[UILabel alloc] initWithFrame:CGRectMake(14, 32, 260, 25)];
    [shooterEmail setTextColor:[UIColor darkGrayColor]];
     [shooterEmail setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp.email isEqualToString:@""]){
        shooterEmail.text = @"Email: NA";
    }else{
        shooterEmail.text = [NSString stringWithFormat:@"Email: %@", shooterTemp.email];
    }
  
    UILabel *shooterEmergencyContact = [[UILabel alloc] initWithFrame:CGRectMake(14, 55, 240, 25)];
    [shooterEmergencyContact setTextColor:[UIColor darkGrayColor]];
     [shooterEmergencyContact setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp.emergencyContact isEqualToString:@""]){
        shooterEmergencyContact.text = @"E-Contact: NA";
    }else{
        shooterEmergencyContact.text = [NSString stringWithFormat:@"E-Contact: %@", shooterTemp.emergencyContact];
    }

    UILabel *shooterEmergencyPhone = [[UILabel alloc] initWithFrame:CGRectMake(14, 80, 240, 25)];
    [shooterEmergencyPhone setTextColor:[UIColor darkGrayColor]];
     [shooterEmergencyPhone setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp.emergencyContactNum isEqualToString:@""]){
        shooterEmergencyPhone.text = @"E-Phone: NA";
    }else{
        shooterEmergencyPhone.text = [NSString stringWithFormat:@"E-Phone: %@", shooterTemp.emergencyContactNum];
    }


    [cell addSubview:shooterEmergencyContact];
    [cell addSubview:shooterEmergencyPhone];
    [cell addSubview:shooterEmail];
    [cell addSubview:shooterPhone];
    [cell addSubview: cellTitle];
    return cell;
}


#pragma mark TAB BAR DELEGATE

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if([item.title isEqualToString:@"Score Card" ]){
        
        [self presentViewController:scoringPage
                           animated:YES
                         completion:nil];
        
    }
    else if([item.title isEqualToString:@"Settings"]){
        
        [self presentViewController:settingsPage
                           animated:YES
                         completion:nil];
        
    }
    else{
        
        
    
    }
}


- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
    
        if([self isNetworkAvailable]){
            
            
            
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        // Retrieve the first person who have the given firstname
        NSString *objectId = [[shootersArray objectAtIndex:indexPath.row] objectId];
            self.indexPathToDelete = indexPath;
        self.shooterToDelete = [Shooter MR_findFirstByAttribute:@"objectId" withValue:objectId inContext:localContext];
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Shooter" message:[NSString stringWithFormat: @"This will delete all records of this shooter and related shoots. Are you sure you want to delete %@ %@ from your team?", shooterToDelete.fName, shooterToDelete.lName] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"DELETE", nil];
            
            [alert show];
            alert.tag = 1;
            
                }
        else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to delete a shooter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        
    }
}

#pragma mark ADD SHOOTER VIEW
//This action will flip the tableview to reveal the add shooter form
- (IBAction)addShooter:(id)sender {
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpAddShooterView];
                        [self.container addSubview: self.addShooterView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];
    
    
}


-(void) setUpAddShooterView {
    
    
    [self.addShooterView setBackgroundColor:[UIColor lightGrayColor]];
    self.navBar.prompt = nil;
    self.cancelAddShooterOutlet.enabled = YES;
    self.addShooterOutlet.enabled = NO;
    
     UIButton *addShooter;
    
    if(self.view.frame.size.width > 320)
    {
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 60, 280, 30)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 20, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 100, 280, 30)];
        emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 140, 280, 30)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 180, 280, 30)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 220, 280, 30)];
        chokeType = [[UISegmentedControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 310, 280, 40)];
        addShooter = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 400, 150, 50)];
    }
    else{
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(20, 60, 280, 30)];
           firstName = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(20, 100, 280, 30)];
         emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(20, 140, 280, 30)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(20, 180, 280, 30)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(20, 220, 280, 30)];
         chokeType = [[UISegmentedControl alloc] initWithFrame: CGRectMake(20, 310, 280, 40)];
          addShooter = [[UIButton alloc] initWithFrame:CGRectMake(90, 400, 150, 50)];
    }

    [self.chokeType setBackgroundColor:[UIColor lightGrayColor]];
    [self.chokeType setTintColor:[UIColor darkGrayColor]];
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setReturnKeyType:UIReturnKeyNext];
    [firstName setBackgroundColor:[UIColor whiteColor]];
    [firstName setBorderStyle:UITextBorderStyleRoundedRect];
    [firstName setPlaceholder:@"First Name"];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
   
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackgroundColor:[UIColor whiteColor]];
    [lastName setBorderStyle:UITextBorderStyleRoundedRect];
    [lastName setPlaceholder:@"Last Name"];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor whiteColor]];
    [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setPlaceholder:@"Phone Number"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setReturnKeyType:UIReturnKeyNext];
    [emailAddress setBackgroundColor:[UIColor whiteColor]];
    [emailAddress setBorderStyle:UITextBorderStyleRoundedRect];
    [emailAddress setPlaceholder:@"Email Address (Required)"];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [emergencyContact setKeyboardType:UIKeyboardTypeDefault];
    [emergencyContact setReturnKeyType:UIReturnKeyNext];
    [emergencyContact setBackgroundColor:[UIColor whiteColor]];
    [emergencyContact setBorderStyle:UITextBorderStyleRoundedRect];
    [emergencyContact setPlaceholder:@"Emergency Contact (Optional)"];
    [emergencyContact addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emergencyContactNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [emergencyContactNum setReturnKeyType:UIReturnKeyDone];
    [emergencyContactNum setBackgroundColor:[UIColor whiteColor]];
    [emergencyContactNum setBorderStyle:UITextBorderStyleRoundedRect];
    [emergencyContactNum setPlaceholder:@"E-Contact Phone # (Optional)"];
    [emergencyContactNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [chokeType insertSegmentWithTitle:@"Full" atIndex:0 animated:NO];
    [chokeType insertSegmentWithTitle:@"Modified" atIndex:1 animated:NO];
    [chokeType insertSegmentWithTitle:@"Improved" atIndex:2 animated:NO];
    [chokeType setSelectedSegmentIndex:1];
  


   
    [addShooter setTitle:@"Add Shooter" forState:UIControlStateNormal];
    [addShooter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addShooter.titleLabel setFont:[UIFont boldSystemFontOfSize: 20]];
    [addShooter.titleLabel sizeToFit];
    [addShooter addTarget:self action:@selector(addShooter) forControlEvents:UIControlEventTouchUpInside];
    [addShooter setEnabled:true];
    
    /*UIButton *cancelAddShooter = [[UIButton alloc] initWithFrame:CGRectMake(40, 400, 100, 50)];
    [cancelAddShooter setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelAddShooter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelAddShooter.titleLabel setFont:[UIFont fontWithName:@"CaviarDreams-Bold" size:20]];
    [cancelAddShooter.titleLabel sizeToFit];
    [cancelAddShooter addTarget:self action:@selector(cancelAddShooter) forControlEvents:UIControlEventTouchUpInside];
    [cancelAddShooter setEnabled:true];*/
    
    [self.addShooterView addSubview:emergencyContact];
    [self.addShooterView addSubview:emergencyContactNum];
    [self.addShooterView addSubview:firstName];
    [self.addShooterView addSubview:lastName];
    [self.addShooterView addSubview:phoneNum];
    [self.addShooterView addSubview:emailAddress];
  
    [self.addShooterView addSubview:chokeType];
    [self.addShooterView addSubview:addShooter];
     //[self.firstName becomeFirstResponder];
  //  [self.addShooterView addSubview:cancelAddShooter];
}


//Adds shooter to parse and core data
-(void) addShooter {
   
    if(firstName.text.length < 1 || lastName.text.length < 1 || self.emailAddress.text.length < 4)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
  
    NSString *choke= [self.chokeType titleForSegmentAtIndex:[self.chokeType selectedSegmentIndex]];

    NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
    
    self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    Shooter *shooter = [Shooter MR_createInContext:self.managedObjectContext];
    shooter.fName = self.firstName.text;
    shooter.lName = self.lastName.text;
    shooter.phone = self.phoneNum.text;
    shooter.email = self.emailAddress.text;
    shooter.emergencyContact = self.emergencyContact.text;
    shooter.emergencyContactNum = self.emergencyContactNum.text;
    shooter.choke = choke;
    shooter.userId = [PFUser currentUser].objectId;
    //int x = [Shooter MR_countOfEntities];
    shooter.objectId = tempHashId;//[NSNumber numberWithInt:(x + 1)];
    
    [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            
            PFObject *shooterObject = [PFObject objectWithClassName:@"Shooter"];
            shooterObject[@"appObjectId"] = tempHashId;
            shooterObject[@"fName"] = self.firstName.text;
            shooterObject[@"lName"] = self.lastName.text;
            shooterObject[@"phone"] = self.phoneNum.text;
            shooterObject[@"email"] = self.emailAddress.text;
            shooterObject[@"emergencyContact"] = self.emergencyContact.text;
            shooterObject[@"emergencyContactNum"] = self.emergencyContactNum.text;
            shooterObject[@"choke"] = choke;
            shooterObject[@"userId"] = [PFUser currentUser].objectId;
            
            [shooterObject saveEventually:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    
                    
                    
                    NSLog(@"SavedShooterToParse");
                    
                    
                }
            }];
            
            
        }
    }];
    
    [self.shootersArray removeAllObjects];
    
    self.shootersArray = [[Shooter MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
   
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
 
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:Nil waitUntilDone:NO ];
    self.addShooterOutlet.enabled = YES;
     self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
        
    }
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}

-(void) nextOnKeyboard : (id) sender {
    
   if(sender == self.firstName)
   {
       [self.firstName resignFirstResponder];
       [self.lastName becomeFirstResponder];
   }
   else if(sender == self.lastName){
       
       [self.lastName resignFirstResponder];
       [self.self.phoneNum becomeFirstResponder];
   }
   else if(sender == self.phoneNum){
       
       [self.phoneNum resignFirstResponder];
       [self.emailAddress becomeFirstResponder];
   }
   else if (sender == self.emailAddress){
       
       [self.emailAddress resignFirstResponder];
       [self.emergencyContact becomeFirstResponder];
       
   }
   else if (sender == self.emergencyContact){
       
       [self.emergencyContact resignFirstResponder];
       [self.emergencyContactNum becomeFirstResponder];
       
       
   }
   else{
       
       [self.emergencyContactNum resignFirstResponder];
       
   }
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

-(void) tapGesture {
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    [self.emergencyContact resignFirstResponder];
    [self.emergencyContactNum resignFirstResponder];
}
- (IBAction)cancelAddShooter:(id)sender {
    
    NSLog(@"Cancel Add Shooter");
    
    for(UIView *view in self.addShooterView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    self.addShooterOutlet.enabled = YES;
     self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
    [self.tableView setEditing:NO animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView moreOptionButtonPressedInRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"EDIT CLICKED");
    if([self isNetworkAvailable]){
       
        [self editShooter: [self.shootersArray objectAtIndex:indexPath.row]];
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to edit this shooter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
    
}


- (NSString *)tableView:(UITableView *)tableView titleForMoreOptionButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return @"Edit";
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [self.tableView setEditing:NO animated:YES];
    
    if(buttonIndex == 1){
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSString *objectId = self.shooterToDelete.objectId;
    [shooterToDelete MR_deleteInContext:localContext];
   
    NSMutableArray *shootShooters = [[ShootShooters MR_findByAttribute:@"shooterId" withValue:objectId] mutableCopy];
        NSMutableArray *shooterIdArray = [[NSMutableArray alloc] init];

        for(int x = 0; x < shootShooters.count; x++){
            
            ShootShooters *shootShooter = [shootShooters objectAtIndex:x];
            [shooterIdArray addObject:[shootShooters objectAtIndex:x]];
            [shootShooter MR_deleteInContext:localContext];
            
            
        }

    
    [localContext MR_saveToPersistentStoreWithCompletion: ^(BOOL success, NSError *error) {
        if(success){
            
            PFQuery *shootShooterQuery = [PFQuery queryWithClassName:@"ShootShooters"];
            [shootShooterQuery whereKey:@"shooterID" equalTo:objectId];
                
            
            [shootShooterQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    NSLog(@"Successfully retrieved %d scores.", objects.count);
                    // Do something with the found objects
                    for(int x = 0; x < objects.count; x++){
                        
                        PFObject *shootShooter = [objects objectAtIndex:x];
                        [shootShooter deleteEventually];
                        
                    }
                   
                    
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
            
            //NEED TO DELETE SHOOT IF NOT SHOOTERS LEFT
            
          
            
            
            NSLog(@"ShootShooters: %@", shooterIdArray);
            
            PFQuery *shooterQuery = [PFQuery queryWithClassName:@"Shooter"];
            [shooterQuery whereKey:@"appObjectId" equalTo:objectId];
            [shooterQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    NSLog(@"Successfully retrieved %d scores.", objects.count);
                    // Do something with the found objects
                    PFObject *shooter = [objects objectAtIndex:0];
                    [shooter deleteEventually];
                    
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        }
    }];
    
        NSMutableArray *shoots = [[StoredShoot MR_findAll] mutableCopy];
        for(int x = 0; x < shoots.count; x++){
            StoredShoot *tempStoredShoot = [shoots objectAtIndex:x];
            
            NSMutableArray *tempShootShooters = [[ShootShooters MR_findByAttribute:@"storedShootId" withValue:tempStoredShoot.objectId] mutableCopy];
            
            if(tempShootShooters.count == 0){
                
                PFQuery *query = [PFQuery queryWithClassName:@"Shoot"];
                [query whereKey:@"appObjectId" equalTo:tempStoredShoot.objectId];
                [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    
                    if(objects){
                        NSLog(@"Deleting Shoot Becasue of 0 shooters");
                        
                        [[objects objectAtIndex:0] deleteEventually];
                        
                        [tempStoredShoot MR_deleteInContext:localContext];
                        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                            if(success)
                            {
                                NSLog(@"Deleted Local Shoot becasue of 0 shooters");
                            }
                        }];
                        
                        
                    }
                    
                }];
                
            }
        }

    // [self.updatedData getShooters];
    
    
        [self.shootersArray removeObjectAtIndex:self.indexPathToDelete.row];
    [self.tableView reloadData];
}
    
    
}

-(void)editShooter : (Shooter *)shooter{
    
    
    for(UIView *view in self.addShooterView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }

    
    self.editShooter = shooter;
    [self.addShooterView setBackgroundColor:[UIColor lightGrayColor]];
    self.navBar.prompt = nil;
    self.cancelAddShooterOutlet.enabled = YES;
    self.addShooterOutlet.enabled = NO;
    
    UIButton *addShooter;
    
    if(self.view.frame.size.width > 320)
    {
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 60, 280, 30)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 20, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 100, 280, 30)];
        emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 140, 280, 30)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 180, 280, 30)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 220, 280, 30)];
        chokeType = [[UISegmentedControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 310, 280, 40)];
        addShooter = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 400, 150, 50)];
    }
    else{
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(20, 60, 280, 30)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(20, 100, 280, 30)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(20, 140, 280, 30)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(20, 180, 280, 30)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(20, 220, 280, 30)];
        chokeType = [[UISegmentedControl alloc] initWithFrame: CGRectMake(20, 310, 280, 40)];
        addShooter = [[UIButton alloc] initWithFrame:CGRectMake(90, 400, 150, 50)];
    }
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setReturnKeyType:UIReturnKeyNext];
    [firstName setBackgroundColor:[UIColor whiteColor]];
    [firstName setBorderStyle:UITextBorderStyleRoundedRect];
    [firstName setPlaceholder:@"First Name"];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackgroundColor:[UIColor whiteColor]];
    [lastName setBorderStyle:UITextBorderStyleRoundedRect];
    [lastName setPlaceholder:@"Last Name"];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor whiteColor]];
    [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setPlaceholder:@"Phone Number"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setReturnKeyType:UIReturnKeyNext];
    [emailAddress setBackgroundColor:[UIColor whiteColor]];
    [emailAddress setBorderStyle:UITextBorderStyleRoundedRect];
    [emailAddress setPlaceholder:@"Email Address (Optional)"];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [emergencyContact setKeyboardType:UIKeyboardTypeDefault];
    [emergencyContact setReturnKeyType:UIReturnKeyNext];
    [emergencyContact setBackgroundColor:[UIColor whiteColor]];
    [emergencyContact setBorderStyle:UITextBorderStyleRoundedRect];
    [emergencyContact setPlaceholder:@"Emergency Contact (Optional)"];
    [emergencyContact addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emergencyContactNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [emergencyContactNum setReturnKeyType:UIReturnKeyDone];
    [emergencyContactNum setBackgroundColor:[UIColor whiteColor]];
    [emergencyContactNum setBorderStyle:UITextBorderStyleRoundedRect];
    [emergencyContactNum setPlaceholder:@"E-Contact Phone # (Optional)"];
    [emergencyContactNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [chokeType insertSegmentWithTitle:@"Full" atIndex:0 animated:NO];
    [chokeType insertSegmentWithTitle:@"Modified" atIndex:1 animated:NO];
    [chokeType insertSegmentWithTitle:@"Improved" atIndex:2 animated:NO];
    [chokeType setSelectedSegmentIndex:1];
    
    
    
    [addShooter setTitle:@"Save Shooter" forState:UIControlStateNormal];
    [addShooter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addShooter.titleLabel setFont:[UIFont boldSystemFontOfSize: 20]];
    [addShooter.titleLabel sizeToFit];
    [addShooter addTarget:self action:@selector(editShooterSave) forControlEvents:UIControlEventTouchUpInside];
    [addShooter setEnabled:true];
    
    self.firstName.text = shooter.fName;
    self.lastName.text = shooter.lName;
    self.phoneNum.text = shooter.phone;
    self.emailAddress.text = shooter.email;
    self.emergencyContact.text = shooter.emergencyContact;
    self.emergencyContactNum.text = shooter.emergencyContactNum;
    
    if([shooter.choke isEqualToString:@"Full"]){
        [self.chokeType setSelectedSegmentIndex:0];
    
    }else if([shooter.choke isEqualToString:@"Modified"]){
        
        [self.chokeType setSelectedSegmentIndex:1];
    }
    else{
        
        [self.chokeType setSelectedSegmentIndex:1];
    }
    
    
    
    [self.addShooterView addSubview:emergencyContact];
    [self.addShooterView addSubview:emergencyContactNum];
    [self.addShooterView addSubview:firstName];
    [self.addShooterView addSubview:lastName];
    [self.addShooterView addSubview:phoneNum];
    [self.addShooterView addSubview:emailAddress];
    
    [self.addShooterView addSubview:chokeType];
    [self.addShooterView addSubview:addShooter];
    
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self.container addSubview: self.addShooterView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];

    
}

-(void) editShooterSave {

    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSString *objectId = self.editShooter.objectId;
    Shooter *shooter = [Shooter MR_findFirstByAttribute:@"objectId" withValue:objectId];

    
    
    
    if(firstName.text.length < 1 || lastName.text.length < 1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    
    NSString *choke= [self.chokeType titleForSegmentAtIndex:[self.chokeType selectedSegmentIndex]];
    
   // NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
        
    shooter.fName = self.firstName.text;
    shooter.lName = self.lastName.text;
    shooter.phone = self.phoneNum.text;
    shooter.email = self.emailAddress.text;
    shooter.emergencyContact = self.emergencyContact.text;
    shooter.emergencyContactNum = self.emergencyContactNum.text;
    shooter.choke = choke;
 
        
       
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            
           
            PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
            [query whereKey:@"appObjectId" equalTo:self.editShooter.objectId];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(objects){
                    
                    [objects objectAtIndex:0][@"fName"] = self.firstName.text;
                    [objects objectAtIndex:0][@"lName"] = self.lastName.text;
                    [objects objectAtIndex:0][@"phone"] = self.phoneNum.text;
                   [objects objectAtIndex:0][@"email"] = self.emailAddress.text;
                    [objects objectAtIndex:0][@"emergencyContact"] = self.emergencyContact.text;
                    [objects objectAtIndex:0][@"emergencyContactNum"] = self.emergencyContactNum.text;
                    [objects objectAtIndex:0][@"choke"] = choke;

                    [[objects objectAtIndex:0] saveEventually:^(BOOL succeeded, NSError *error) {
                        if(succeeded){
                            
                            
                            
                            NSLog(@"SavedShooterToParse");
                            
                            
                        }
                    }];
                    
                    
                }
            }];
           
            
         
            
        }
    }];
    
    [self.shootersArray removeAllObjects];
    
    self.shootersArray = [[Shooter MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
    
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:Nil waitUntilDone:NO ];
    self.addShooterOutlet.enabled = YES;
    self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
    
    
}
}


@end
