//
//  SetupShootView.m
//  ClayCounter
//
//  Created by ActivateInnovation on 1/30/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SetupShootView.h"

@interface SetupShootView ()

@end

@implementation SetupShootView
@synthesize scoringPage;
@synthesize dateLabel;
@synthesize selectCoachView;
@synthesize container;
@synthesize navBar;
@synthesize beginShootOutlet;
@synthesize cancelSetUpButton;
@synthesize selectCoachTableView;
@synthesize coachArray;
@synthesize updateData;
@synthesize counter;
@synthesize selectedCoach;
@synthesize pracCompSegmentContol;
@synthesize finalShootersTableView;
@synthesize shotsInShootSegmentControl;
@synthesize shotsPerStationSegementControl;
@synthesize shootersSelectedArray;
@synthesize selectShootersView;
@synthesize shootersArray;
@synthesize shootersTableView;
@synthesize container2;
@synthesize shootersCounter;
@synthesize finalShootObject;
@synthesize scoringShootPageView;
@synthesize scoringPageTemp;
@synthesize inTransitPostNumber;
@synthesize shooterCounterLabel;
@synthesize checkmarksArray;
@synthesize numberOfShootersSelected;
@synthesize trapHousePicker;
@synthesize trapRangePicker;
@synthesize trapRanges;
@synthesize trapHouseNumbers;
@synthesize trapRangeLabel;
@synthesize trapHouseLabel;
@synthesize statesArray;
@synthesize selectShootersButton;
@synthesize dragShootersLabel;
@synthesize doneOnPickerOutlet;
@synthesize trapHouseNumberSelected;
@synthesize trapRangeSelected;
@synthesize pickerOpened;
@synthesize selectCoachOutlet;
@synthesize selectTrapHouseOutlet;
@synthesize selectTrapRangeOutlet;
@synthesize handicapSelected;
@synthesize handicapLabel;
@synthesize selectHandicapOutlet;
@synthesize handicapNumbersArray;
@synthesize handicapPicker;
@synthesize yrdsLabel;
@synthesize skipButtonOutlet;
@synthesize stateSelected;
@synthesize addTrapRangeLabel;
@synthesize dontSeeRangeLabel;
@synthesize addTrapRangeView;
@synthesize rangeName;
@synthesize phoneNum;
@synthesize street;
@synthesize city;
@synthesize state;
@synthesize zipCode;
@synthesize statePicker;
@synthesize addRangeButton;
@synthesize addRangeCounter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    NSString *date = [self getDate];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", date];
     self.beginShootOutlet.tag = 1;
    self.updateData = [[UpdatedData alloc] init];
    self.updateData.delegate = self;
    self.cancelSetUpButton.tag = 1;
    self.coachArray = [[NSMutableArray alloc] init];
    self.shootersSelectedArray = [[NSMutableArray alloc] init];
    //self.shootersArray = [[NSMutableArray alloc] init];
    self.checkmarksArray = [[NSMutableArray alloc] init];
    self.numberOfShootersSelected = 0;
     [updateData getShooters];
    [updateData getCoaches];
   
    self.stateSelected = 0;
    self.yrdsLabel.hidden = YES;
    self.pickerOpened = 0;
    self.trapRangeSelected = NO;
    self.trapHouseNumberSelected = NO;
    self.handicapSelected = NO;
    self.doneOnPickerOutlet.hidden = YES;
    self.skipButtonOutlet.hidden = YES;
    self.addTrapRangeLabel.hidden = YES;
    self.dontSeeRangeLabel.hidden = YES;
    self.addRangeCounter = 1;
    
   //Gets rid of any remaining currentShoots 
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSArray *tempShoot = [CurrentShoot MR_findAll];
    
    for(int x = 0; x < tempShoot.count; x++){
        
        CurrentShoot *temp = [tempShoot objectAtIndex:x];
        [temp MR_deleteInContext:localContext];
    }
    
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if(success) {
            NSLog(@"Success deleting object");
            
            
        }
    }];

    
    [self.coachArray addObjectsFromArray: updateData.coachesArray];
    for(int x = 0; x < self.coachArray.count; x++)
    {
        
        [self.checkmarksArray insertObject: [NSNumber numberWithInt:0] atIndex:x];
        
    }
    
    self.statesArray = [[NSMutableArray alloc] initWithObjects: @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH", @"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];

    //Setting up data for both pickers
    self.trapHouseNumbers = [[NSMutableArray alloc] init];
    for(int x = 1; x < 101; x++){
        
        [self.trapHouseNumbers addObject:[NSString stringWithFormat:@"%d", x]];
    }
    
    self.handicapNumbersArray = [[NSMutableArray alloc] init];
    for(int x = 16; x < 28; x++){
        
        [self.handicapNumbersArray addObject:[NSString stringWithFormat:@"%d", x]];
    }

   
    
     //  NSLog(@"Trap Ranges start: %@", self.trapRanges);
   // self.trapRanges = [[TrapField MR_findAllSortedBy:@"name" ascending:YES] mutableCopy];
    
 
       self.container = [[UIView alloc] init];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];

    
    self.selectCoachTableView = [[UITableView alloc] init];
    self.selectCoachTableView.delegate = self;
  
    self.selectCoachTableView.dataSource = self;
    
    self.selectCoachView = [[UIView alloc] init];
    self.selectCoachView.clearsContextBeforeDrawing = YES;
      [self.selectCoachView setBackgroundColor:[UIColor lightGrayColor]];
    self.scoringPageTemp = [[ScoringShootPage alloc] init];
    
    
    self.addTrapRangeView = [[UIView alloc] init];
    self.addTrapRangeView.clearsContextBeforeDrawing = YES;
    self.scoringPageTemp = [[ScoringShootPage alloc] init];
    
    
    self.counter = 0;
    self.shootersCounter = 1;
    self.inTransitPostNumber = 0;
   
    self.cancelSetUpButton.tag = 1;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];

    
    self.scoringShootPageView =
    [storyboard instantiateViewControllerWithIdentifier:@"ScoringShootPage"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)beginShoot:(id)sender {

    
    if([sender tag] == 1){
    
        if(self.shootersSelectedArray.count == 0 || self.selectedCoach == nil || self.trapRangeSelected == NO || self.trapHouseNumberSelected == NO || handicapSelected == NO){
            
           // NSLog(@"Not all data here");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure to have a coach, a value for the trap range (or \"None Selected\"), a trap house number, a handicap distance, and at least one shooter selected before beginning." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            self.beginShootOutlet.tag = 1;
            
        }
        //This checks to make sure that when 25, or 75 shots selected that only 5 shots perpost can be selected. 
        else if(([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"25"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"10"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"75"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"10"])){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect Data" message:@"Only 5 shots per post are allowed when shooting 25 shots." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        
            
        }
        else{
            
           // NSLog(@"Begin Shoot: %d", self.shootersSelectedArray.count);
            //[self presentViewController: scoringPage animated:YES completion:nil];
            
            NSString *finalStartingPosts = [[NSString alloc] init];
          
            int tempCounter = 0;
            
            NSMutableArray *indexesToDelete = [[NSMutableArray alloc] init];
            
            for(int x = 0; x < self.shootersSelectedArray.count; x++)
            {
                  
              if([[self.shootersSelectedArray objectAtIndex:x] isKindOfClass:[Shooter class]]){
                  
                //  NSLog(@"TEST");
                  
                  if(tempCounter == 0){
                      // NSLog(@"TEST1");
                      finalStartingPosts = [NSString stringWithFormat:@"%d", (x + 1)];
                      tempCounter++;
                  }
                  else{
                      
                      // NSLog(@"TEST2");
                      finalStartingPosts = [NSString stringWithFormat:@"%@, %d",finalStartingPosts, (x + 1)];
                      tempCounter++;
                  }
                  
              }
            
              else
              {
                  [indexesToDelete addObject:[self.shootersSelectedArray objectAtIndex:x] ];
              }
           
            
            }
            
           // NSLog(@"Selected Shooters: %@", self.shootersSelectedArray);
          //  NSLog(@"Indexes: %@", indexesToDelete);
            for(int j = 0; j < indexesToDelete.count; j++)
            {
                for(int x = 0; x < self.shootersSelectedArray.count; x++)
                {
                    if([self.shootersSelectedArray[x] isEqual: indexesToDelete[j]]){
                        
                        [self.shootersSelectedArray removeObjectAtIndex:x];
                        
                    }
                    
                }
                
               
            }
    
            
        
           //NSLog(@"FINALPOSTS: %@", finalStartingPosts);
            
            self.finalShootObject = [[ShootObjectClass alloc] initWithItems:self.shootersSelectedArray : self.selectedCoach :[[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] intValue] : [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] intValue]];
            NSString *tempString = [[NSString alloc] init];
            Shooter *tempShooter = [self.shootersSelectedArray objectAtIndex:0];
            tempString = [NSString stringWithFormat:@"%@", tempShooter.objectId];
                          
            for(int x = 1; x < [self.shootersSelectedArray count]; x++){
                
                Shooter *temp = [self.shootersSelectedArray objectAtIndex:x];
                tempString = [NSString stringWithFormat:@"%@,%@", tempString, temp.objectId];
                
           // NSLog(@"tempString: %@", tempString);
            }
            
            self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
            TrapField *trapField;
        
            
            NSNumber *finalTrapHouse = [NSNumber numberWithInt:[self.trapHouseLabel.text intValue]];
            
            CurrentShoot *currentShoot = [CurrentShoot MR_createInContext:self.managedObjectContext];
            currentShoot.coach = [NSString stringWithFormat:@"%@", self.selectedCoach.objectId];
            currentShoot.startingPost = finalStartingPosts;
            currentShoot.shooters = tempString;
            currentShoot.shotsAtPost = [NSNumber numberWithInt:self.finalShootObject.shotsAtEachPost] ;
            currentShoot.totalShots = [NSNumber numberWithInt:self.finalShootObject.shotsTotal];
            if([self.trapRangeLabel.text isEqualToString:@"None Selected"]){
                
                 currentShoot.trapFieldId = @"NA";
                
            }
            else{
                
                trapField = [TrapField MR_findFirstByAttribute:@"name" withValue:self.trapRangeLabel.text];
                 currentShoot.trapFieldId = trapField.objectId;
            }

            currentShoot.handicap = [NSNumber numberWithInt:[self.handicapLabel.text intValue]];
            currentShoot.trapHouseNumber = finalTrapHouse;
            currentShoot.pracOrComp = [self.pracCompSegmentContol titleForSegmentAtIndex:[self.pracCompSegmentContol selectedSegmentIndex]];
        
            [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if (success) {
                    NSLog(@"Success");
                    
                }
            }];

          
            // [scoringPageTemp setShootData:self.finalShootObject];
            
            [self presentViewController:self.scoringShootPageView animated: YES completion:nil];
            
            //NEED TO SEND THE SHOOTOBJECT TO THE SET SHOOT DATA METHOD IN THE SCORINGSHOOTPAGE AND TRANSITON THE VIEW
            
           // NSLog(@"Prac/Comp: %@, ShotsTotal: %@, Shots/Station: %@", [self.pracCompSegmentContol titleForSegmentAtIndex:[self.pracCompSegmentContol selectedSegmentIndex]], [self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]], [self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]]);
            
            //NSLog(@"Selected Shooters: %@", self.shootersSelectedArray);
        //    NSLog(@"Selected Coach: %@", selectedCoach);
        
            
        }
    }
    
    else if([sender tag] == 2){
        
        
       // NSLog(@"selectedShooters: %lu", (unsigned long)[self.shootersSelectedArray count]);
        [self doneClickedOnAddShooters];
        
    }
    else{
       
        
        
        
    }
    
    

    
    
}

-(void) retrieveData{
    
    
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (counter == 1) {
       
        return [self.coachArray count];
   }
    else if(counter == 2){
        
         return [self.coachArray count];
        
    }
    else if(tableView == self.finalShootersTableView){
        
        return [self.shootersSelectedArray count];
        
    }
    NSLog(@"HERE");
    return 0;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: nil];
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    if (cell == nil) {
        
        NSLog(@"New");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if(tableView == self.selectCoachTableView && counter == 2){
            if([[self.checkmarksArray objectAtIndex:indexPath.row] intValue] == 1)
            {
                
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];

            
            }
            else{
            
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            }
        }
        else{
            
             [cell setAccessoryType:UITableViewCellAccessoryNone];
            
        }
    }
    
  
    
    if(counter == 1){
        
    Coach *coachTemp = [self.coachArray objectAtIndex:indexPath.row];
    
    UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 180, 30)];
    [cellTitle setText: [NSString stringWithFormat:@"%@ %@",coachTemp.fName, coachTemp.lName]];
    
    [cell addSubview: cellTitle];
    
    }
    else if(counter == 2) {
        
       
        Shooter *shooterTemp = [self.coachArray objectAtIndex:indexPath.row];
        UILabel *cellTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 180, 30)];
        [cell addSubview: cellTitle2];
        [cellTitle2 setText: [NSString stringWithFormat:@"%@ %@", shooterTemp.fName, shooterTemp.lName]];
        


    }
    else if(tableView == self.finalShootersTableView){
        
        UILabel *cellTitle3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 180, 30)];
        [cell addSubview: cellTitle3];
        [cell setBackgroundColor: [UIColor clearColor]];
        
        if([[self.shootersSelectedArray objectAtIndex:indexPath.row] isKindOfClass: [NSNumber class]]) {
        
            
            [cellTitle3 setText: @""];
            
           
        }
        else
        {
         
            Shooter *shooterTemp = [self.shootersSelectedArray objectAtIndex:indexPath.row];
            UILabel *position = [[UILabel alloc] initWithFrame:CGRectMake(190, 10, 75, 25)];
            [cell addSubview:position];
            [position setText:[NSString stringWithFormat:@"Post #%ld", (long)(indexPath.row + 1) ]];
            [position setFont:[UIFont systemFontOfSize:14]];
            [position setTextColor:[UIColor darkGrayColor]];
            
            [cellTitle3 setText: [NSString stringWithFormat:@"%@ %@", shooterTemp.fName, shooterTemp.lName]];

        }
        
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSLog(@"Counter %d", counter);
    
    UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    if(tableView == self.finalShootersTableView){
        
        self.finalShootersTableView.editing = YES;
        if(thisCell.accessoryType == UITableViewCellAccessoryNone) {
            
            thisCell.showsReorderControl = YES;
        }

        
    }
    
    else if(counter == 1){
        if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
      
        thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
        Coach *coachTemp = [coachArray objectAtIndex:indexPath.row];
        self.selectedCoach = coachTemp;
        self.coachNameLabel.text = [NSString stringWithFormat:@": %@ %@",coachTemp.fName, coachTemp.lName];
        [self getCoach];
    
        
        }else{
    	thisCell.accessoryType = UITableViewCellAccessoryNone;
        self.coachNameLabel.text = @"";
          
        
        }
    }else if(counter == 2){
        
        //SET THE CHECk MARK NUMBER
        if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
            NSLog(@"AT NOt Accessory");
            if(self.shootersCounter < 6){
      
                    //UILabel *counterAccessory = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 20)];
                    //counterAccessory.text = [NSString stringWithFormat:@"%d", self.shootersCounter];
                    //[counterAccessory sizeToFit];
                    thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
              
                    [self.checkmarksArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:1]];
                
           
                
                
                    self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", self.shootersCounter];
                    // thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    [self.shootersSelectedArray addObject:[self.coachArray objectAtIndex: indexPath.row]];
                
                    NSLog(@"%lu", (unsigned long)[self.shootersSelectedArray count]);
                
                    self.shootersCounter++;
                
                }
           
            }
            
        else{
            NSLog(@"AT Accessory");
           
            
            if(self.shootersCounter != 0){
                
                thisCell.accessoryType = UITableViewCellAccessoryNone;
            [self.checkmarksArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:0]];
                [thisCell setSelected:NO];
                //NEED TO CHECK THE SHOOTERS ARRAY AND REMOVE THE RIGHT ONE IF ONE IS UNSELECTED
                [self.shootersSelectedArray removeObjectIdenticalTo: [self.coachArray objectAtIndex: indexPath.row]];
                
                
                [self.finalShootersTableView reloadData];
                self.shootersCounter--;
                 self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", (self.shootersCounter - 1)];
              
                
                
            }
            
        }

    }
    
}



- (IBAction)cancelClicked:(id)sender {
    
    self.pracCompSegmentContol.hidden = NO;
    self.dateLabel.hidden = NO;
    
    if([sender tag] == 1){
        
        [self presentViewController: scoringPage animated:YES completion:nil];
    }
    else if ([sender tag] == 2){
        
        NSLog(@"selectedShooters: %lu", (unsigned long)[self.shootersSelectedArray count]);
        [self.beginShootOutlet setEnabled:YES];
        [self.shootersSelectedArray removeAllObjects];
       
        
        if(self.shootersSelectedArray.count < 5 && self.shootersSelectedArray.count > 0){
            
            for(int j = self.shootersSelectedArray.count; j < 5 ; j++){
                
                NSLog(@"X: %d", j);
                NSNumber *j = [NSNumber numberWithInt:0];
                [self.shootersSelectedArray addObject: j];
                
                
            }
        }

        self.beginShootOutlet.title = @"Begin Shoot!";
        self.navBar.topItem.title = @"Setup Shoot";
        [self.navBar.topItem setPrompt:nil];
        self.shootersCounter = 1;
        [self.beginShootOutlet setEnabled: YES];
       
        self.beginShootOutlet.tag = 1;
        self.cancelSetUpButton.tag = 1;
        
        for(UIView *view in self.selectCoachView.subviews){
            if ([view isKindOfClass:[UIView class]]) {
                [view removeFromSuperview];
            }
        }
        
        [UIView transitionWithView:self.container
                          duration:0.6
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:^{
                            [self.selectCoachView removeFromSuperview];
                          
                        }
                        completion:NULL];
        
        [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
        
          self.counter = 0;
        [self.finalShootersTableView reloadData];
            
    }
    else{
        
       
        [self.beginShootOutlet setEnabled:YES];
        self.beginShootOutlet.title = @"Begin Shoot!";
        self.navBar.topItem.title = @"Setup Shoot";
        //[self.navBar.topItem setPrompt:nil];
        [self.beginShootOutlet setEnabled: YES];
        self.beginShootOutlet.tag = 1;
        self.cancelSetUpButton.tag = 1;
        
        for(UIView *view in self.addTrapRangeView.subviews){
            if ([view isKindOfClass:[UIView class]]) {
                [view removeFromSuperview];
            }
        }
        
        [UIView transitionWithView:self.container
                          duration:0.6
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:^{
                            [self.addTrapRangeView removeFromSuperview];
                            
                        }
                        completion:NULL];
        
        [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
        
    }
}



-(void) doneClickedOnAddShooters{
    
    self.beginShootOutlet.title = @"Begin Shoot!";
    self.navBar.topItem.prompt = nil;
    self.pracCompSegmentContol.hidden = NO;
    self.dateLabel.hidden = NO;
    self.beginShootOutlet.tag = 1;
    self.beginShootOutlet.enabled = NO;
    self.cancelSetUpButton.tag = 1;
    self.finalShootersTableView.delegate = self;
    self.finalShootersTableView.dataSource = self;
    self.shootersCounter = 1;
    [self.selectCoachTableView removeFromSuperview];
    
    if(self.shootersSelectedArray.count == 0){
        
        [self.shootersSelectedArray removeAllObjects];
        
    }
    for(UIView *view in self.selectCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.selectCoachView removeFromSuperview];
                      
                        
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    
    self.counter = 0;
    self.numberOfShootersSelected = self.shootersSelectedArray.count;
    
    NSLog(@"selectedShootersArray1: %@", self.shootersSelectedArray);
    
    if(self.shootersSelectedArray.count < 5 && self.shootersSelectedArray.count > 0){
        
        for(int j = self.shootersSelectedArray.count; j < 5 ; j++){
            
                NSLog(@"X: %d", j);
                NSNumber *j = [NSNumber numberWithInt:0];
                [self.shootersSelectedArray addObject: j];
           
            
        }
        
        NSLog(@"selectedShootersArray2: %@", self.shootersSelectedArray);
        
        
    }
    
    
    [self.finalShootersTableView reloadData];
    self.beginShootOutlet.enabled = YES;
    self.finalShootersTableView.editing = YES;
    
}

-(NSString*) getDate {
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    dateString = [formatter stringFromDate:[NSDate date]];

    return dateString;
}


- (IBAction)selectCoach:(id)sender {
    
    self.counter = 1;
   
     self.beginShootOutlet.title = @"Begin Shoot!";

    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpSelectCoachView];
                        [self.container addSubview: self.selectCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion: nil];

    
}


-(void) setUpSelectCoachView{
    
    self.navBar.topItem.title = @"Select Coach";
   
 
    [self.container setFrame: CGRectMake(0, 61, self.view.frame.size.width, self.view.frame.size.height)];
    [self.selectCoachView setFrame:self.container.bounds];
    [self.selectCoachTableView setFrame: self.selectCoachView.bounds];
    self.cancelSetUpButton.tag = 2;
    self.beginShootOutlet.tag = 2;
    [self.beginShootOutlet setEnabled:NO];
   [self.selectCoachTableView deselectRowAtIndexPath:[self.selectCoachTableView indexPathForSelectedRow] animated:NO];
    
    [self.coachArray removeAllObjects];
    [self.coachArray addObjectsFromArray: updateData.coachesArray];
    [self.selectCoachView setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.selectCoachView addSubview:self.selectCoachTableView];
    [self.selectCoachTableView reloadData];
    
    
}

-(void) getCoach {
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.selectCoachView removeFromSuperview];
                        [self.selectCoachTableView removeFromSuperview];
                        [self.beginShootOutlet setEnabled:YES];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    
    self.navBar.topItem.title = @"Setup Shoot";
    self.counter = 0;
    self.cancelSetUpButton.tag = 1;
    self.beginShootOutlet.tag = 1;
    

    
    
}
- (IBAction)selectShooters:(id)sender {
    
    self.counter = 2;
     self.shootersCounter = 1;
    
    [self.shootersSelectedArray removeAllObjects];
    NSLog(@"%lu", (unsigned long)self.shootersSelectedArray.count);
    [self.selectCoachTableView reloadData];

    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpSelectShooterView];
                        [self.container addSubview: self.selectCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion: nil];
    
    
}


-(void) setUpSelectShooterView {
    
    //self.counter = 2;
    self.pracCompSegmentContol.hidden = YES;
    self.dateLabel.hidden = YES;
    self.navBar.topItem.title = @"Select Shooters";
    self.navBar.topItem.prompt = @"Select up to 5 shooters";
    self.beginShootOutlet.title = @"DONE";
    [self.container setFrame: CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height)];
    [self.selectCoachView setFrame:self.container.bounds];
    [self.selectCoachTableView setFrame: CGRectMake(0, 30, self.container.frame.size.width, self.container.frame.size.height)];
    self.shooterCounterLabel = [[UILabel alloc] initWithFrame: CGRectMake(60, 3 , 200, 28)];
    [self.shooterCounterLabel setTextAlignment:NSTextAlignmentCenter];
    [self.shooterCounterLabel setFont:[UIFont boldSystemFontOfSize:16]];
    self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", 0];
   self.cancelSetUpButton.tag = 2;
    self.beginShootOutlet.tag = 2;
    [self.coachArray removeAllObjects];
    [self.coachArray addObjectsFromArray: updateData.shootersArray];
    for(int x = 0; x < self.coachArray.count; x++)
    {
       
        [self.checkmarksArray insertObject: [NSNumber numberWithInt:0] atIndex:x];
        
    }
    //self.beginShootOutlet.tag = 2;
   [self.beginShootOutlet setEnabled:YES];
    //[self.shootersTableView deselectRowAtIndexPath:[self.shootersTableView indexPathForSelectedRow] animated:NO];
    //[self.selectCoachView addSubview:selectCoachTableView];
    //[self.selectShootersView setBackgroundColor:[UIColor lightGrayColor]];
    [self.selectCoachView addSubview: self.shooterCounterLabel];
    [self.selectCoachView addSubview:self.selectCoachTableView];
    [self.selectCoachTableView reloadData];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

-(UITableViewCellEditingStyle) tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleNone;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    
    id tempData = [shootersSelectedArray objectAtIndex: sourceIndexPath.row];
    [shootersSelectedArray replaceObjectAtIndex:sourceIndexPath.row withObject:[shootersSelectedArray objectAtIndex:destinationIndexPath.row]];
   [shootersSelectedArray replaceObjectAtIndex: destinationIndexPath.row withObject:tempData];
    
    
   // [shootersSelectedArray replaceObjectAtIndex: sourceIndexPath.row withObject: [shootersSelectedArray objectAtIndex: destinationIndexPath.row]];
    NSLog(@"SelectedShooters3: %@", self.shootersSelectedArray);
    
    [self.finalShootersTableView reloadData];
    
}

#pragma mark PICKERVIEW DELEGATE

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    if(pickerView == self.trapRangePicker){
        
        return 2;
    }
    else{
       
        return 1;
    }
    
}



// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
            return [self.statesArray count];
        }
        else{
            return [self.trapRanges count];
        }
        
    }
    else if(pickerView == self.trapHousePicker) //Trap house Picker
    {
        
        return [self.trapHouseNumbers count];
        
    }
    else if(pickerView == self.handicapPicker){
        
        return [self.handicapNumbersArray count];
        
    }
    else{
        return [self.statesArray count];
    }
    
}
/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = [[NSString alloc] init];
    
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
             title = [self.statesArray objectAtIndex:row];
        }
        else{
            TrapField *temp = [self.trapRanges objectAtIndex:row];
            title = temp.name;
            
        }
       
    }
    else //Trap house Picker
    {
        title = [self.trapHouseNumbers objectAtIndex:row];
        
    }
    
     return title;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
 {
 return (80.0);
 }*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
            
            //Need to check size and add an object that says there are no fields here so it doesnt crash
            NSString *stateTemp = [self.statesArray objectAtIndex:row];
            self.stateSelected = row;
            [self.trapRanges removeAllObjects];
            self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateTemp andOrderBy:@"name" ascending:YES] mutableCopy];
            if(self.trapRanges.count == 0)
            {
                //[self.trapRanges removeAllObjects];
                [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", stateTemp]];
                self.trapRangeLabel.text = @"None Selected";
                [self.trapRangePicker reloadAllComponents];
            }
            else{
                
                
                
                TrapField *temp = [self.trapRanges objectAtIndex:0];
                self.trapRangeLabel.text = temp.name;
                [self.trapRangePicker reloadAllComponents];
            }
            
        }
        else{
            
            if([[self.trapRanges objectAtIndex:row] isKindOfClass:[TrapField class]] )
            {
                TrapField *temp = [self.trapRanges objectAtIndex:row];
                self.trapRangeLabel.text = temp.name;
                [self.trapRangePicker reloadAllComponents];
            }
            else{
                
                self.trapRangeLabel.text = @"None Selected";
            }

            
        }

    }
    else if(pickerView == self.trapHousePicker) //Trap house Picker
    {
        self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [self.trapHouseNumbers objectAtIndex:row]];
        
    }
    else if(pickerView == self.handicapPicker){ //handicap Picker
        
        self.handicapLabel.text = [NSString stringWithFormat:@"%@", [self.handicapNumbersArray objectAtIndex:row]];
        
    }
    else{ //statepicker
      
        
        self.state.text = [NSString stringWithFormat:@"%@", [self.statesArray objectAtIndex:row]];
        
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
   
            
            if(pickerView == self.trapRangePicker){
                
                if(component == 0){
                    
                    return 60;
                }
                else{
                    
                    return 260;
                    
                }
            }
            else{//If it is the trap house number picker or handicap
                
                return 33;
            }
}
  
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *pickerLabel = (UILabel *)view;
	// Reuse the label if possible, otherwise create and configure a new one
	if ((pickerLabel == nil) || ([pickerLabel class] != [UILabel class]))
    {  //newlabel
		
		pickerLabel = [[UILabel alloc] init];
		
		pickerLabel.backgroundColor = [UIColor clearColor];
        [pickerLabel sizeToFit];
    }
	pickerLabel.textColor = [UIColor blackColor];
    NSString*  text = @"";
    
    if(pickerView == self.trapRangePicker){
        
        if (component == 0)
        {
            [pickerLabel setFrame: CGRectMake(15.0, 0.0, 46, 32.0)];
            pickerLabel.textAlignment = NSTextAlignmentCenter;
            text = [self.statesArray objectAtIndex:row];
        }
        else
        {
            [pickerLabel setFrame: CGRectMake(50.0, 0.0, 270, 32.0)];
            pickerLabel.textAlignment = NSTextAlignmentCenter;
            if([[self.trapRanges objectAtIndex:row] isKindOfClass:[TrapField class]] )
            {
                TrapField *temp = [self.trapRanges objectAtIndex:row];
                text = temp.name;
            }
            else{
                
                text = [self.trapRanges objectAtIndex:0];
            }
            
         
        }

    }
    else if(pickerView == self.trapHousePicker){
        
        text = [self.trapHouseNumbers objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    else if(pickerView == self.handicapPicker){
       
        text = [self.handicapNumbersArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    else{
        
        text = [self.statesArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    pickerLabel.text = text;
    
	return pickerLabel;
}


- (IBAction)selectTrapRange:(id)sender {
    
    self.selectShootersButton.enabled = NO;
    self.selectTrapRangeOutlet.enabled = NO;
    self.selectCoachOutlet.enabled = NO;
    self.selectTrapHouseOutlet.enabled = NO;
    self.beginShootOutlet.enabled = NO;
    self.trapRangePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 270, 320, 216)];
    self.trapRangePicker.delegate = self;
    self.trapRangePicker.dataSource = self;
    self.trapRangePicker.showsSelectionIndicator = YES;
    self.finalShootersTableView.hidden = YES;
    self.selectShootersButton.hidden = YES;
    self.dragShootersLabel.hidden = YES;
    self.doneOnPickerOutlet.hidden = NO;
    self.skipButtonOutlet.hidden = NO;
    self.pickerOpened = 1;
    self.addTrapRangeLabel.hidden = NO;
    self.dontSeeRangeLabel.hidden = NO;
    
    
    [self.trapRanges removeAllObjects];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL defaultOrNO = [userDefaults boolForKey:@"defaultOrNot"];
    if(defaultOrNO == NO)
    {
        
        NSLog(@"No Default");
        NSString *stateTemp = [self.statesArray objectAtIndex:0];
        self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateTemp andOrderBy:@"name" ascending:YES] mutableCopy];
        if(trapRanges.count == 0)
        {
            
            [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", state]];
        }
    }
    else{
        
        NSLog(@"Default");
        int tempState = [userDefaults integerForKey:@"defaultState"];
        NSString *stateString = [self.statesArray objectAtIndex:tempState];
        self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateString andOrderBy:@"name" ascending:YES] mutableCopy];
        [self.trapRangePicker selectRow:tempState inComponent:0 animated:YES];
        if(trapRanges.count == 0)
        {
            
            [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", stateString]];
        }
        
        
    }

    
    if(self.trapRanges.count == 1){
       
        if([[self.trapRanges objectAtIndex:0] isKindOfClass:[TrapField class]] )
        {
            TrapField *temp = [self.trapRanges objectAtIndex:0];
            self.trapRangeLabel.text = temp.name;
        }
        else{
            
            self.trapRangeLabel.text = @"None Selected";
        }

    }
    else{
        
        TrapField *temp = [self.trapRanges objectAtIndex:0];
        self.trapRangeLabel.text = temp.name;
    }
    
    [self.view addSubview: self.trapRangePicker];
    [self.trapRangePicker reloadAllComponents];
}


//NEED TO ADD TAG CHECKING - 1 FOR HOUSE - 2 FOR HANDICAP
- (IBAction)selectTrapHouse:(id)sender {
    
    self.selectShootersButton.enabled = NO;
    self.selectTrapRangeOutlet.enabled = NO;
    self.selectCoachOutlet.enabled = NO;
    self.selectTrapHouseOutlet.enabled = NO;
    self.beginShootOutlet.enabled = NO;
    self.selectHandicapOutlet.enabled = NO;
    self.finalShootersTableView.hidden = YES;
    self.selectShootersButton.hidden = YES;
    self.dragShootersLabel.hidden = YES;
    self.doneOnPickerOutlet.hidden = NO;

    
    if([sender tag] == 1){
    
        self.trapHousePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 270, 320, 216)];
    self.trapHousePicker.delegate = self;
    self.trapHousePicker.dataSource = self;
        self.trapHousePicker.showsSelectionIndicator = YES;
    self.pickerOpened = 2;

    self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [self.trapHouseNumbers objectAtIndex:0]];
    [self.view addSubview: self.trapHousePicker];
    }
    else{//HANDICAP PICKER
        
        self.handicapPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 270, 320, 216)];
        self.handicapPicker.delegate = self;
        self.handicapPicker.dataSource = self;
        self.handicapPicker.showsSelectionIndicator = YES;
        self.pickerOpened = 3;
        
        self.handicapLabel.text = [NSString stringWithFormat:@"%@", [self.handicapNumbersArray objectAtIndex:0]];
        self.yrdsLabel.hidden = NO;
        [self.view addSubview: self.handicapPicker];
        
        
        
    }
    
    
}

- (IBAction)doneOnPicker:(id)sender {
    
    
    
    if(self.pickerOpened == 1){
        
        self.addTrapRangeLabel.hidden = YES;
        self.dontSeeRangeLabel.hidden = YES;
        //Prompts the user to set a default state
        self.trapRangeSelected = YES;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        BOOL defaultOrNO = [userDefaults boolForKey:@"defaultOrNot"];
        if(defaultOrNO == NO)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Default State" message:[NSString stringWithFormat:@"Would you like to set %@ as your default state?",[self.statesArray objectAtIndex:self.stateSelected] ] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.tag = 2;
            [alert show];
            
        }

        
    }
    else if(pickerOpened == 2){
        
        self.trapHouseNumberSelected = YES;
    }
    else{
        
        self.handicapSelected = YES;
    }
    
    self.selectShootersButton.enabled = YES;
    self.selectTrapRangeOutlet.enabled = YES;
    self.selectCoachOutlet.enabled = YES;
    self.selectTrapHouseOutlet.enabled = YES;
    self.beginShootOutlet.enabled = YES;
    self.selectHandicapOutlet.enabled = YES;
    self.finalShootersTableView.hidden = NO;
    self.selectShootersButton.hidden = NO;
    self.dragShootersLabel.hidden = NO;
    self.doneOnPickerOutlet.hidden = YES;
    self.skipButtonOutlet.hidden = YES;
    [self.trapHousePicker removeFromSuperview];
    [self.trapRangePicker removeFromSuperview];
    [self.handicapPicker removeFromSuperview];
    
}
- (IBAction)skipTrapRange:(id)sender {
    
    if(self.pickerOpened == 1){
        
        self.trapRangeSelected = YES;
    }

    self.trapRangeLabel.text = @"None Selected";
    
    self.addTrapRangeLabel.hidden = YES;
    self.dontSeeRangeLabel.hidden = YES;
    self.selectShootersButton.enabled = YES;
    self.selectTrapRangeOutlet.enabled = YES;
    self.selectCoachOutlet.enabled = YES;
    self.selectTrapHouseOutlet.enabled = YES;
    self.beginShootOutlet.enabled = YES;
    self.selectHandicapOutlet.enabled = YES;
    self.finalShootersTableView.hidden = NO;
    self.selectShootersButton.hidden = NO;
    self.dragShootersLabel.hidden = NO;
    self.doneOnPickerOutlet.hidden = YES;
    self.skipButtonOutlet.hidden = YES;
    [self.trapRangePicker removeFromSuperview];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 2){
        
        if(buttonIndex == 0){
            
            NSLog(@"0");
            
        }
        else{  //YES clicked on Default state for Trap Ranges
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setBool:YES forKey:@"defaultOrNot"];
            [userDefaults setInteger:self.stateSelected forKey:@"defaultState"];
        }
        
    }
}
- (IBAction)addTrapRange:(id)sender {
    
    NSLog(@"Add Trap Range");
    
    if([self isNetworkAvailable]){
        
    self.counter = 3;
    
    self.beginShootOutlet.title = @"Begin Shoot!";
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpAddTrapRangeView];
                        [self.container addSubview: self.addTrapRangeView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion: nil];
    }
    else{ //No Network Detected
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"This feature requires an internet connection. Please connect and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
        
    }
}

-(void) setUpAddTrapRangeView {
    
   
    self.pracCompSegmentContol.hidden = YES;
    self.dateLabel.hidden = YES;
    self.navBar.topItem.title = @"Add Trap Range";
   // self.navBar.topItem.prompt = @"";
    //self.beginShootOutlet.title = @"DONE";
    [self.container setFrame: CGRectMake(0, 55, self.view.frame.size.width, self.view.frame.size.height)];
    [self.addTrapRangeView setFrame:self.container.bounds];
    [self.addTrapRangeView setBackgroundColor:[UIColor lightGrayColor]];
    [self.addTrapRangeView setFrame: CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)];
    self.addRangeCounter = 1;
    self.cancelSetUpButton.tag = 3;
    self.beginShootOutlet.tag = 3;
    [self.beginShootOutlet setEnabled:NO];
    
   /* UILabel *internetLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, 280, 25)];
    [internetLabel setTextAlignment:NSTextAlignmentCenter];
    [internetLabel setText:@"(Internet Connection Required)"];
    [internetLabel setTextColor:[UIColor darkGrayColor]];*/
    
    
    if(self.view.frame.size.width > 320)
    {
        rangeName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 25, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 65, 280, 30)];
        street = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 105, 280, 30)];
        city = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 145, 280, 30)];
        state = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 185, 280, 30)];
        zipCode= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 225, 280, 30)];
        addRangeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 260, 150, 50)];
    }
    else{
        rangeName = [[UITextField alloc] initWithFrame:CGRectMake(20,25, 280, 30)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(20, 65, 280, 30)];
        street = [[UITextField alloc] initWithFrame:CGRectMake(20, 105, 280, 30)];
        city = [[UITextField alloc] initWithFrame:CGRectMake(20, 145, 280, 30)];
        state = [[UITextField alloc] initWithFrame:CGRectMake(20, 185, 280, 30)];
        zipCode= [[UITextField alloc] initWithFrame:CGRectMake(20, 225, 280, 30)];
        addRangeButton = [[UIButton alloc] initWithFrame:CGRectMake(85, 255, 150, 50)];
    
    }
    
  
    [rangeName setKeyboardType:UIKeyboardTypeDefault];
    [rangeName setReturnKeyType:UIReturnKeyNext];
    self.rangeName.delegate = self;
    [rangeName setBackgroundColor:[UIColor whiteColor]];
    [rangeName setBorderStyle:UITextBorderStyleRoundedRect];
    [rangeName setPlaceholder:@"Range Name"];
    [rangeName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    phoneNum.delegate = self;
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor whiteColor]];
    [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setPlaceholder:@"Phone Number"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [street setKeyboardType:UIKeyboardTypeDefault];
    street.delegate = self;
    [street setReturnKeyType:UIReturnKeyNext];
    [street setBackgroundColor:[UIColor whiteColor]];
    [street setBorderStyle:UITextBorderStyleRoundedRect];
    [street setPlaceholder:@"Street Address"];
    [street addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [city setKeyboardType:UIKeyboardTypeDefault];
    [city setReturnKeyType:UIReturnKeyNext];
    city.delegate = self;
    [city setBackgroundColor:[UIColor whiteColor]];
    [city setBorderStyle:UITextBorderStyleRoundedRect];
    [city setPlaceholder:@"City"];
    [city addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [state setReturnKeyType:UIReturnKeyDone];
    [state setBackgroundColor:[UIColor whiteColor]];
    [state setBorderStyle:UITextBorderStyleRoundedRect];
    [state setPlaceholder:@"State"];
     self.state.delegate = self;
    [state addTarget:self action:@selector(addSelectState) forControlEvents:UIControlEventEditingDidBegin];
    [state addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidBegin];
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [dummyView setBackgroundColor:[UIColor clearColor]];
    [self.state setInputView: dummyView];
   
    
    
    [zipCode setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [zipCode setReturnKeyType:UIReturnKeyDone];
    [zipCode setBackgroundColor:[UIColor whiteColor]];
    zipCode.delegate = self;
    [zipCode setBorderStyle:UITextBorderStyleRoundedRect];
    [zipCode setPlaceholder:@"Zip Code"];
    [zipCode addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];

    
    
    
    
    [addRangeButton setTitle:@"Add Range" forState:UIControlStateNormal];
    [addRangeButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [addRangeButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [addRangeButton.titleLabel setFont:[UIFont boldSystemFontOfSize: 20]];
    [addRangeButton addTarget:self action:@selector(addRange) forControlEvents:UIControlEventTouchUpInside];
    [addRangeButton setEnabled:true];
    
    /*UIButton *cancelAddShooter = [[UIButton alloc] initWithFrame:CGRectMake(40, 400, 100, 50)];
     [cancelAddShooter setTitle:@"Cancel" forState:UIControlStateNormal];
     [cancelAddShooter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
     [cancelAddShooter.titleLabel setFont:[UIFont fontWithName:@"CaviarDreams-Bold" size:20]];
     [cancelAddShooter.titleLabel sizeToFit];
     [cancelAddShooter addTarget:self action:@selector(cancelAddShooter) forControlEvents:UIControlEventTouchUpInside];
     [cancelAddShooter setEnabled:true];*/
    
    [self.addTrapRangeView addSubview:rangeName];
    [self.addTrapRangeView addSubview:phoneNum];
    [self.addTrapRangeView addSubview:street];
    [self.addTrapRangeView addSubview:city];
    [self.addTrapRangeView addSubview:state];
   // [self.addTrapRangeView addSubview:internetLabel];
    [self.addTrapRangeView addSubview:zipCode];
    [self.addTrapRangeView addSubview:addRangeButton];

    
}

-(void)addRange{
    
    
    NSLog(@"addRange");
    
    if(addRangeCounter == 1)
    {
        
        if(self.rangeName.text.length > 0 && self.phoneNum.text.length > 0 && self.street.text.length > 0 && self.city.text.length > 0 && self.state.text.length > 0 && self.zipCode.text.length > 0){
        
            PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
            [query whereKey:@"Street" equalTo:self.street.text];
            [query whereKey:@"Name" equalTo:self.rangeName.text];
            [query whereKey:@"ZipCode" equalTo:self.zipCode.text];
            NSArray *queryArray = [query findObjects];
            if([queryArray count] != 0)
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Data Error" message:@"There is alredy a range matching these details." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{ //Save to Parse and CoreData
                
                [self.beginShootOutlet setEnabled:YES];
                self.beginShootOutlet.title = @"Begin Shoot!";
                self.navBar.topItem.title = @"Setup Shoot";
                //[self.navBar.topItem setPrompt:nil];
                [self.beginShootOutlet setEnabled: YES];
                self.beginShootOutlet.tag = 1;
                self.cancelSetUpButton.tag = 1;
                // self.addRangeCounter = 1;
    
                
                PFObject *trapField = [PFObject objectWithClassName: @"TrapField"];
                trapField[@"Name"] = self.rangeName.text;
                trapField[@"Phone"] = self.phoneNum.text;
                trapField[@"Street"] = self.street.text;
                trapField[@"City"] = self.city.text;
                trapField[@"State"] = self.state.text;
                trapField[@"ZipCode"] = self.zipCode.text;
                
                [trapField save];
                
                
                //NSMutableArray *tempObjects = [[NSMutableArray alloc] init];
                PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
                [query whereKey:@"Name" equalTo:self.rangeName.text];
                PFObject *tempTrapField =[query getFirstObject];
                
                 self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
          
                TrapField *trapRangeToSave = [TrapField MR_createInContext:self.managedObjectContext];
                
                trapRangeToSave.objectId = tempTrapField.objectId;
                trapRangeToSave.name = tempTrapField[@"Name"];
                trapRangeToSave.phone = tempTrapField[@"Phone"];
                trapRangeToSave.street = tempTrapField[@"Street"];
                trapRangeToSave.city = tempTrapField[@"City"];
                trapRangeToSave.state = tempTrapField[@"State"];
                trapRangeToSave.zipCode = tempTrapField[@"ZipCode"];
                
                [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    if (success) {
                        
                        //  NSLog(@"New Shooter Added to Local Storage");
                        
                    }
                }];

                
                
                for(UIView *view in self.addTrapRangeView.subviews){
                    if ([view isKindOfClass:[UIView class]]) {
                        [view removeFromSuperview];
                    }
                }
    
                [UIView transitionWithView:self.container
                                  duration:0.6
                                   options:UIViewAnimationOptionTransitionFlipFromTop
                                animations:^{
                                    [self.addTrapRangeView  removeFromSuperview];
                        
                                }
                                completion:NULL];
    
            [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
                
                UIAlertView *success = [[UIAlertView alloc] initWithTitle:@"Success" message:@"The new trap range was saved successfully. Thank you!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [success show];
                
                [self.trapRanges removeAllObjects];
                self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:[self.statesArray objectAtIndex:self.stateSelected] andOrderBy:@"name" ascending:YES] mutableCopy];
                if(self.trapRanges.count == 0)
                {
                    //[self.trapRanges removeAllObjects];
                    [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", [self.statesArray objectAtIndex:self.stateSelected] ]];
                    self.trapRangeLabel.text = @"None Selected";
                    [self.trapRangePicker reloadAllComponents];
                }

                [self.trapRangePicker reloadAllComponents];
                
                
            }
        }
        else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"All fields must be filled out correctly before submitting." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
    }
    else{
    
        [self.state setUserInteractionEnabled:YES];
        [self.statePicker removeFromSuperview];
        [self.phoneNum setUserInteractionEnabled: YES];
        [self.zipCode becomeFirstResponder];
        [self.city setUserInteractionEnabled:YES];
        [self.zipCode setUserInteractionEnabled:YES];
        [self.rangeName setUserInteractionEnabled:YES];
        [self.street setUserInteractionEnabled:YES];
        [self.city setUserInteractionEnabled:YES];
        [self.zipCode becomeFirstResponder];
        rangeName.hidden = NO;
        phoneNum.hidden = NO;
        street.hidden = NO;
        city.hidden = NO;
        zipCode.hidden = NO;
        [self.addRangeButton setFrame:CGRectMake(85, 255, 150, 50   )];

        self.addRangeCounter = 1;
        
    }
    
    
  
    
}

-(void) nextOnKeyboard: (id) sender{
    
    
    
    NSLog(@"NextOnKeyboard");
    if(sender == self.rangeName){
        
        [self.rangeName resignFirstResponder];
        [self.phoneNum becomeFirstResponder];
        
    }
    else if(sender == self.phoneNum){
        [self.phoneNum resignFirstResponder];
        [self.street becomeFirstResponder];
        
        
    }
    else if(sender == self.street){
        [self.street resignFirstResponder];
        [self.city becomeFirstResponder];
        
    }
    else if (sender == self.city){
        [self.addTrapRangeView endEditing:YES];
        [self.city resignFirstResponder];
        [self.state resignFirstResponder];
        [self addSelectState];
        
    }
    else if(sender == state){
        [self.city resignFirstResponder];
        [self.state resignFirstResponder];
        
    }
    else{//sender == zipCode
        
        [self.zipCode resignFirstResponder];
        self.addRangeButton.titleLabel.text = @"Add Range";
        self.addRangeCounter = 1;
        [self.statePicker removeFromSuperview];
        
    }
}

-(void) addSelectState {
    
    
    if(addRangeCounter == 2){
        
        
    }
    else{
    
        rangeName.hidden = YES;
        phoneNum.hidden = YES;
        street.hidden = YES;
        city.hidden = YES;
        zipCode.hidden = YES;
        [self.addRangeButton setFrame:CGRectMake(85, 230, 150, 50   )];

        
    NSLog(@"addSelectState");
    
    self.addRangeButton.titleLabel.text = @"DONE";
    
    self.addRangeCounter = 2;
    self.statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, -25, 320, 216)];
    self.statePicker.delegate = self;
    self.statePicker.dataSource = self;
    self.statePicker.showsSelectionIndicator = YES;

    [self.state setUserInteractionEnabled:NO];
    self.state.text = [self.statesArray objectAtIndex:0];
        
    [self.addTrapRangeView endEditing:YES];
        
        
        [self.city setUserInteractionEnabled:NO];
        [self.phoneNum setUserInteractionEnabled:NO];
        [self.zipCode setUserInteractionEnabled:NO];
        [self.rangeName setUserInteractionEnabled:NO];
        [self.street setUserInteractionEnabled:NO];
        [self.city setUserInteractionEnabled:NO];
        [self.state resignFirstResponder];
        [self.addTrapRangeView addSubview:self.statePicker];
    }
    
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}


-(void) textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.state){
   
        rangeName.hidden = YES;
        phoneNum.hidden = YES;
        street.hidden = YES;
        city.hidden = YES;
        zipCode.hidden = YES;
        [self.addRangeButton setFrame:CGRectMake(85, 230, 150, 50   )];
        
    }
}



@end
