//
//  SetupShootView.h
//  ClayCounter
//
//  Created by ActivateInnovation on 1/30/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoringPage.h"
#import "UpdatedData.h"
#import "Coach.h"
#import "Shooter.h"
#import "ShootObjectClass.h"
#import "ScoringShootPage.h"
#import "CurrentShoot.h"
#import "TrapField.h"


@class Coach;
@class ScoringPage;
@class UpdatedData;
@class Shooter;
@class ShootObjectClass;
@class ScoringShootPage;
@class CurrentShoot;

@interface SetupShootView : UIViewController <UITableViewDelegate, UITableViewDataSource, UpdateDataDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
- (IBAction)beginShoot:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *beginShootOutlet;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
- (IBAction)cancelClicked:(id)sender;
@property ScoringPage *scoringPage;
@property (strong, nonatomic) ScoringShootPage *scoringShootPageView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *pracCompSegmentContol;
@property (strong, nonatomic) IBOutlet UILabel *coachNameLabel;
@property (strong, nonatomic) UILabel *shooterCounterLabel;
@property (strong, nonatomic) UIView *container;
@property (strong, nonatomic) UIView *selectCoachView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong , nonatomic) UIView *selectShootersView;
@property (strong , nonatomic) UIView *addTrapRangeView;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelSetUpButton;
@property int numberOfShootersSelected;
- (IBAction)selectCoach:(id)sender;
@property int inTransitPostNumber;
@property (strong, nonatomic) UITableView *selectCoachTableView;
@property (strong, nonatomic) NSMutableArray *coachArray;
@property UpdatedData *updateData;
@property int counter;
@property (strong, nonatomic) Coach *selectedCoach;
@property (strong, nonatomic) IBOutlet UISegmentedControl *shotsInShootSegmentControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *shotsPerStationSegementControl;
@property (strong, nonatomic) IBOutlet UILabel *dragShootersLabel;
@property (strong, nonatomic) IBOutlet UIButton *selectShootersButton;
@property (strong, nonatomic) UIPickerView *trapRangePicker;
@property (strong, nonatomic) UIPickerView *trapHousePicker;
- (IBAction)selectShooters:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *finalShootersTableView;
@property int shootersCounter;
@property (strong, nonatomic) ScoringShootPage *scoringPageTemp;
@property (strong, nonatomic) NSMutableArray *shootersSelectedArray;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) UITableView *shootersTableView;
@property (strong, nonatomic) UIView *container2;
@property (strong, nonatomic) ShootObjectClass *finalShootObject;
@property (strong, nonatomic) NSMutableArray *checkmarksArray;
@property (strong, nonatomic) NSMutableArray *trapRanges;
@property (strong, nonatomic) NSMutableArray *trapHouseNumbers;
- (IBAction)selectTrapRange:(id)sender;
- (IBAction)selectTrapHouse:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *trapRangeLabel;
@property (strong, nonatomic) IBOutlet UILabel *trapHouseLabel;
@property (strong, nonatomic) NSMutableArray *statesArray;
@property (strong, nonatomic) IBOutlet UIButton *doneOnPickerOutlet;
@property (strong, nonatomic) UIPickerView *handicapPicker;
- (IBAction)doneOnPicker:(id)sender;
@property BOOL trapRangeSelected;
@property BOOL trapHouseNumberSelected;
@property int pickerOpened; 
@property (strong, nonatomic) IBOutlet UIButton *selectTrapHouseOutlet;
@property (strong, nonatomic) IBOutlet UIButton *selectTrapRangeOutlet;
@property (strong, nonatomic) IBOutlet UIButton *selectCoachOutlet;
@property (strong, nonatomic) IBOutlet UIButton *selectHandicapOutlet;
@property BOOL handicapSelected;
@property (strong, nonatomic) IBOutlet UILabel *handicapLabel;
@property (strong, nonatomic) NSMutableArray *handicapNumbersArray;
@property (strong, nonatomic) IBOutlet UILabel *yrdsLabel;
- (IBAction)skipTrapRange:(id)sender;
@property (nonatomic) int stateSelected;
@property (strong, nonatomic) IBOutlet UIButton *skipButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *addTrapRangeLabel;
- (IBAction)addTrapRange:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *dontSeeRangeLabel;
@property (strong, nonatomic) UITextField *rangeName;
@property (strong, nonatomic) UITextField *phoneNum;
@property (strong, nonatomic) UITextField *street;
@property (strong, nonatomic) UITextField *city;
@property (strong, nonatomic) UITextField *state;
@property (strong, nonatomic) UITextField *zipCode;
@property (strong, nonatomic) UIPickerView *statePicker;
@property (strong, nonatomic) UIButton *addRangeButton;
@property int addRangeCounter;

@end


